-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 18, 2020 at 04:12 AM
-- Server version: 5.6.43
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jamali`
--

-- --------------------------------------------------------

--
-- Table structure for table `appointments`
--

CREATE TABLE `appointments` (
  `appointment_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `vendor_id` int(10) UNSIGNED NOT NULL,
  `vendor_service_id` int(10) UNSIGNED NOT NULL,
  `user_name` varchar(225) DEFAULT NULL,
  `phone_number` varchar(225) DEFAULT NULL,
  `email` varchar(225) DEFAULT NULL,
  `service_name` varchar(225) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `appointment_date` date DEFAULT NULL,
  `appointment_time` time DEFAULT NULL,
  `status` enum('0','1','2','3','4','5') NOT NULL,
  `payment_status` enum('0','1','3') NOT NULL DEFAULT '3',
  `payment_mode` enum('COD','PAYPAL','CARD','') NOT NULL DEFAULT 'COD',
  `service_cost` double DEFAULT NULL,
  `booking_amount` double DEFAULT NULL,
  `tax` double DEFAULT NULL,
  `payment_method` varchar(11) DEFAULT 'COD',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `appointments`
--

INSERT INTO `appointments` (`appointment_id`, `user_id`, `vendor_id`, `vendor_service_id`, `user_name`, `phone_number`, `email`, `service_name`, `price`, `appointment_date`, `appointment_time`, `status`, `payment_status`, `payment_mode`, `service_cost`, `booking_amount`, `tax`, `payment_method`, `created_at`, `updated_at`, `deleted_at`) VALUES
(11, 28, 20, 12, 'ramlal', '8349877407', NULL, 'Ahh', 12.00, '2020-11-26', '12:00:00', '1', '3', 'COD', NULL, NULL, NULL, 'COD', '2020-11-18 01:43:30', '2020-12-11 08:29:43', NULL),
(12, 13, 31, 15, 'Asim55', '0533755044', 'asimalmotelq@yahoo.com', 'Makeup', 275.00, '2020-12-01', '06:30:00', '4', '3', 'COD', NULL, NULL, NULL, 'COD', '2020-11-19 22:48:13', '2020-11-25 21:26:36', NULL),
(13, 20, 20, 12, 'raju', '9852147965', 'raju@gmail.com', 'Ahh', 12.00, '2020-11-24', '10:00:00', '3', '3', 'COD', NULL, NULL, NULL, 'COD', '2020-11-20 06:32:08', '2020-11-20 06:40:45', NULL),
(14, 20, 20, 12, 'raju', '9852147965', 'raju@gmail.com', 'Ahh', 12.00, '2020-11-24', '10:00:00', '3', '3', 'COD', NULL, NULL, NULL, 'COD', '2020-11-20 06:32:35', '2020-11-20 06:35:21', NULL),
(15, 20, 20, 18, 'raju', '9852147965', 'raju@gmail.com', 'Saloon', 132.00, '2020-11-26', '10:15:00', '3', '3', 'COD', NULL, NULL, NULL, 'COD', '2020-11-20 06:43:58', '2020-11-20 06:49:41', NULL),
(16, 20, 20, 18, 'raju', '9852147965', 'raju@gmail.com', 'Saloon', 132.00, '2020-11-25', '10:00:00', '3', '3', 'COD', NULL, NULL, NULL, 'COD', '2020-11-20 06:49:04', '2020-11-20 06:51:42', NULL),
(17, 20, 20, 18, 'raju', '9852147965', 'raju@gmail.com', 'Saloon', 132.00, '2020-11-25', '03:00:00', '1', '3', 'COD', NULL, NULL, NULL, 'COD', '2020-11-20 07:57:10', '2020-11-25 01:30:36', NULL),
(18, 13, 31, 17, 'Asim55', '0533755044', 'asimalmotelq@yahoo.com', 'Tattoos', 660.00, '2020-12-01', '03:00:00', '2', '3', 'COD', NULL, NULL, NULL, 'COD', '2020-11-23 21:23:50', '2020-11-25 21:23:41', NULL),
(19, 28, 20, 18, 'ramlal', '8349877407', NULL, 'Saloon', 132.00, '2020-11-25', '10:00:00', '4', '3', 'COD', NULL, NULL, NULL, 'COD', '2020-11-25 01:18:20', '2020-11-25 01:25:58', NULL),
(20, 28, 20, 18, 'ramlal', '8349877407', NULL, 'Saloon', 132.00, '2020-11-25', '10:00:00', '1', '3', 'COD', NULL, NULL, NULL, 'COD', '2020-11-25 01:18:21', '2020-11-25 01:18:53', NULL),
(21, 13, 31, 16, 'Asim55', '0533755044', 'asimalmotelq@yahoo.com', 'Lips', 121.00, '2020-11-29', '06:45:00', '4', '3', 'COD', NULL, NULL, NULL, 'COD', '2020-11-25 13:50:17', '2020-11-25 13:51:22', NULL),
(22, 13, 31, 15, 'Asim55', '0533755044', 'asimalmotelq@yahoo.com', 'Makeup', 302.50, '2020-11-29', '05:00:00', '1', '3', 'COD', NULL, NULL, NULL, 'COD', '2020-11-25 21:46:44', '2020-11-25 21:47:32', NULL),
(23, 35, 34, 21, 'Vipin', '9584741316', NULL, 'Hair cuting', 20.00, '2020-11-29', '01:45:00', '1', '3', 'COD', NULL, NULL, NULL, 'COD', '2020-11-26 08:34:30', '2020-11-26 08:34:53', NULL),
(24, 35, 34, 23, 'Vipin', '9584741316', NULL, 'Face bridal womens', 658.90, '2020-11-29', '07:30:00', '4', '3', 'COD', NULL, NULL, NULL, 'COD', '2020-11-26 08:50:41', '2020-11-26 08:52:15', NULL),
(25, 35, 20, 19, 'Vipin', '9584741316', NULL, 'Beauty', 110.00, '2020-11-30', '10:00:00', '1', '3', 'COD', NULL, NULL, NULL, 'COD', '2020-11-26 09:42:46', '2020-11-26 09:43:06', NULL),
(26, 28, 20, 19, 'ramlal', '8349877407', 'ramlal@gmail.com', 'Beauty', 110.00, '2020-11-29', '10:00:00', '1', '3', 'COD', NULL, NULL, NULL, 'COD', '2020-11-27 04:26:08', '2020-11-27 04:26:25', NULL),
(27, 13, 31, 15, 'Asim55', '0533755044', 'asimalmotelq@yahoo.com', 'Makeup', 302.50, '2020-11-29', '06:30:00', '1', '3', 'COD', NULL, NULL, NULL, 'COD', '2020-11-27 11:18:58', '2020-11-27 11:20:40', NULL),
(28, 13, 31, 24, 'Asim55', '0533755044', 'asimalmotelq@yahoo.com', 'Nails', 110.00, '2020-11-29', '05:00:00', '1', '3', 'COD', NULL, NULL, NULL, 'COD', '2020-11-27 13:20:34', '2020-11-27 13:21:46', NULL),
(29, 10, 20, 18, 'ambuj', '645645645', 'ambuj@gmail.com', 'Saloon', 132.00, '2020-12-17', '10:00:00', '3', '3', 'COD', NULL, NULL, NULL, 'COD', '2020-12-07 01:09:52', '2020-12-07 01:18:43', NULL),
(30, 10, 20, 18, 'ambuj', '645645645', 'ambuj@gmail.com', 'Saloon', 132.00, '2020-12-19', '10:00:00', '3', '3', 'COD', NULL, NULL, NULL, 'COD', '2020-12-07 01:24:44', '2020-12-07 01:31:02', NULL),
(31, 10, 20, 19, 'ambuj', '645645645', 'ambuj@gmail.com', 'Beauty', 110.00, '2020-12-26', '01:00:00', '3', '3', 'COD', NULL, NULL, NULL, 'COD', '2020-12-07 01:27:56', '2020-12-07 01:29:30', NULL),
(32, 10, 20, 18, 'ambuj', '645645645', 'ambuj@gmail.com', 'Saloon', 132.00, '2020-12-19', '10:15:00', '2', '3', 'COD', NULL, NULL, NULL, 'COD', '2020-12-07 01:31:40', '2020-12-07 01:31:59', NULL),
(33, 10, 20, 18, 'ambuj', '645645645', 'ambuj@gmail.com', 'Saloon', 132.00, '2020-12-23', '10:00:00', '1', '3', 'COD', NULL, NULL, NULL, 'COD', '2020-12-07 06:39:19', '2020-12-08 02:12:21', NULL),
(34, 28, 20, 25, 'ramlal', '8349877407', 'ramlal@gmail.com', 'Hair red color', 55.00, '2020-12-08', '10:30:00', '4', '3', 'COD', 50, 5, 8.25, 'COD', '2020-12-08 02:24:02', '2020-12-08 02:26:05', NULL),
(35, 28, 20, 18, 'ramlal', '8349877407', 'ramlal@gmail.com', 'Saloon', 132.00, '2020-12-30', '10:00:00', '3', '3', 'COD', 0, 0, 19.8, 'COD', '2020-12-11 01:19:12', '2020-12-11 01:34:11', NULL),
(36, 28, 20, 27, 'ramlal', '8349877407', 'ramlal@gmail.com', 'Hair cuting', 209.00, '2020-12-12', '10:00:00', '4', '3', 'COD', 190, 19, 31.35, 'COD', '2020-12-11 06:20:47', '2020-12-11 06:22:16', NULL),
(37, 6, 20, 28, 'User1', '8959070299', 'user1@mailinator.com', 'Hair was', 218.90, '2020-12-15', '11:15:00', '4', '3', 'COD', 199, 20, 32.835, 'COD', '2020-12-14 05:30:46', '2020-12-14 05:32:55', NULL),
(38, 40, 39, 30, 'Inspiring', '7974682508', '7974682508', 'Codemeg Gold', 165.00, '2020-12-20', '10:00:00', '1', '3', 'COD', 150, 15, 24.75, 'COD', '2020-12-14 08:06:55', '2020-12-14 08:35:09', NULL),
(39, 40, 39, 30, 'Inspiring', '7974682508', '7974682508', 'Codemeg Gold', 165.00, '2020-12-20', '10:00:00', '2', '3', 'COD', 150, 15, 24.75, 'COD', '2020-12-14 08:26:11', '2020-12-14 08:35:31', NULL),
(40, 40, 39, 29, 'Inspiring', '7974682508', '7974682508', 'Codemeg Silver', 110.00, '2020-12-20', '10:15:00', '1', '3', 'COD', 100, 10, 16.5, 'COD', '2020-12-14 08:29:35', '2020-12-14 08:35:39', NULL),
(41, 40, 39, 30, 'Inspiring', '7974682508', '7974682508', 'Codemeg Gold', 165.00, '2020-12-20', '10:15:00', '1', '3', 'COD', 150, 15, 24.75, 'COD', '2020-12-14 08:33:35', '2020-12-14 08:39:50', NULL),
(42, 40, 39, 30, 'Inspiring', '7974682508', '7974682508', 'Codemeg Gold', 165.00, '2020-12-20', '11:00:00', '0', '3', 'COD', 150, 15, 24.75, 'COD', '2020-12-14 08:39:36', '2020-12-14 08:39:36', NULL),
(43, 28, 20, 32, 'ramlal', '8349877407', 'ramlal@gmail.com', 'Facail plus hair wash', 548.90, '2021-01-16', '11:45:00', '4', '3', 'COD', 499, 50, 82.335, 'COD', '2020-12-15 09:11:31', '2020-12-15 09:13:49', NULL),
(44, 42, 41, 33, 'Jhgfdsa', '0533335555', '0533335555', 'Makeup', 110.00, '2020-12-16', '03:45:00', '4', '3', 'COD', 100, 10, 16.5, 'COD', '2020-12-15 15:36:15', '2020-12-15 15:37:37', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `app_ratings`
--

CREATE TABLE `app_ratings` (
  `app_rating_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `rating` int(10) UNSIGNED DEFAULT NULL,
  `review` varchar(225) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `app_ratings`
--

INSERT INTO `app_ratings` (`app_rating_id`, `user_id`, `rating`, `review`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 7, 1, 'best', '2020-10-09 02:56:50', '2020-10-09 02:56:50', '2020-10-09 12:26:50');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `category_id` int(10) UNSIGNED NOT NULL,
  `category_name` varchar(225) DEFAULT NULL,
  `category_image` varchar(225) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`category_id`, `category_name`, `category_image`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Eyebrows', '3.png', '2020-09-15 11:19:18', '2020-10-31 10:51:10', NULL),
(2, 'Eyelashes', '2.png', '2020-09-15 11:19:18', '2020-10-31 10:51:17', NULL),
(3, 'Hairstyles', '4.png', '2020-09-15 11:19:18', '2020-10-31 10:51:25', NULL),
(4, 'Haircut', '1.png', '2020-09-15 11:19:18', '2020-10-31 10:51:34', NULL),
(5, 'Makeup', '5.png', '2020-09-15 11:19:18', '2020-10-31 10:51:42', NULL),
(8, 'Spa', 'kf05E3JV9d_1600578997.png', '2020-09-20 01:05:02', '2020-10-31 10:51:50', NULL),
(9, 'Tattoos', '4.png', '2020-09-15 11:19:18', '2020-10-31 10:51:25', NULL),
(10, 'Painting', '1.png', '2020-09-15 11:19:18', '2020-10-31 10:51:34', NULL),
(11, 'Lips', '5.png', '2020-09-15 11:19:18', '2020-10-31 10:51:42', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `chat`
--

CREATE TABLE `chat` (
  `chat_id` int(10) UNSIGNED NOT NULL,
  `chat_unique_id` varchar(225) NOT NULL,
  `sender_id` int(10) UNSIGNED NOT NULL,
  `receiver_id` int(10) UNSIGNED NOT NULL,
  `message` text NOT NULL,
  `read_status` enum('0','1') NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chat`
--

INSERT INTO `chat` (`chat_id`, `chat_unique_id`, `sender_id`, `receiver_id`, `message`, `read_status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'chat16021381204054', 6, 7, 'Hi Vendor', '0', '2020-10-07 20:52:00', '2020-10-07 20:52:00', NULL),
(2, 'chat16021381204054', 6, 7, 'Hi Vendor', '0', '2020-10-07 20:52:12', '2020-10-07 20:52:12', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `commissions`
--

CREATE TABLE `commissions` (
  `commission_id` int(10) UNSIGNED NOT NULL,
  `type` enum('1','2','3','4') NOT NULL DEFAULT '1',
  `price` decimal(10,2) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `commissions`
--

INSERT INTO `commissions` (`commission_id`, `type`, `price`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '1', 30.00, '2020-02-12 10:59:53', '2020-02-12 10:59:53', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `days`
--

CREATE TABLE `days` (
  `day_id` int(10) UNSIGNED NOT NULL,
  `day_name` varchar(225) DEFAULT NULL,
  `day_status` enum('0','1') DEFAULT NULL,
  `start_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `days`
--

INSERT INTO `days` (`day_id`, `day_name`, `day_status`, `start_time`, `end_time`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Sunday', '0', '09:45:00', '06:45:00', '2020-09-15 12:23:09', '2020-09-15 12:23:09', NULL),
(2, 'Monday', '0', '09:45:00', '06:45:00', '2020-09-15 12:23:09', '2020-09-15 12:23:09', NULL),
(3, 'Tuesday', '0', '09:45:00', '06:45:00', '2020-09-15 12:23:09', '2020-09-15 12:23:09', NULL),
(4, 'Wednesday', '0', '09:45:00', '06:45:00', '2020-09-15 12:23:09', '2020-09-15 12:23:09', NULL),
(5, 'Thursday', '0', '09:45:00', '06:45:00', '2020-09-15 12:23:09', '2020-09-15 12:23:09', NULL),
(6, 'Friday', '0', '09:45:00', '06:45:00', '2020-09-15 12:23:09', '2020-09-15 12:23:09', NULL),
(7, 'Saturday', '0', '09:45:00', '06:45:00', '2020-09-15 12:23:09', '2020-09-15 12:23:09', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `documents`
--

CREATE TABLE `documents` (
  `document_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `type` enum('1','2') NOT NULL COMMENT '1=Driving Licence,2=Driving Insurance',
  `document` varchar(225) DEFAULT NULL,
  `approve_status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '0=unapproved,1=approved,2=rejected',
  `rejected_reason` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `documents`
--

INSERT INTO `documents` (`document_id`, `user_id`, `type`, `document`, `approve_status`, `rejected_reason`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 7, '1', 'Image1', '0', NULL, NULL, NULL, NULL),
(2, 7, '1', 'Image1', '0', NULL, NULL, NULL, NULL),
(3, 7, '1', 'Image1', '0', NULL, NULL, NULL, NULL),
(4, 7, '1', 'Image1', '0', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `notification_id` int(10) UNSIGNED NOT NULL,
  `sender_id` int(10) UNSIGNED NOT NULL,
  `receiver_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(225) DEFAULT NULL,
  `message` varchar(225) DEFAULT NULL,
  `json_data` text,
  `read_status` enum('0','1') NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`notification_id`, `sender_id`, `receiver_id`, `title`, `message`, `json_data`, `read_status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 6, 7, 'Creatae Order', 'This', 'sss', '1', '2020-10-26 12:46:40', '2020-10-26 03:17:27', NULL),
(2, 20, 28, 'Order Accepted', 'Your order accepted by raju', '{\"key\":\"Order_Accepted\",\"title\":\"Order Accepted\",\"message\":\"Your order accepted by raju\",\"sender_id\":20,\"receiver_id\":28}', '1', '2020-11-17 05:49:45', '2020-12-15 09:12:58', NULL),
(3, 20, 28, 'Order Rejected', 'Your order rejected by raju', '{\"key\":\"Order_Rejected\",\"title\":\"Order Rejected\",\"message\":\"Your order rejected by raju\",\"sender_id\":20,\"receiver_id\":28}', '1', '2020-11-17 05:50:27', '2020-12-15 09:12:58', NULL),
(4, 20, 28, 'Order Accepted', 'Your order accepted by raju', '{\"key\":\"Order_Accepted\",\"title\":\"Order Accepted\",\"message\":\"Your order accepted by raju\",\"sender_id\":20,\"receiver_id\":28}', '1', '2020-11-17 05:51:13', '2020-12-15 09:12:58', NULL),
(5, 20, 28, 'Order Accepted', 'Your order accepted by raju', '{\"key\":\"Order_Accepted\",\"title\":\"Order Accepted\",\"message\":\"Your order accepted by raju\",\"sender_id\":20,\"receiver_id\":28}', '1', '2020-11-18 01:43:55', '2020-12-15 09:12:58', NULL),
(6, 20, 28, 'Order Completed', 'Your order completed by raju', '{\"key\":\"Order_Completed\",\"title\":\"Order Completed\",\"message\":\"Your order completed by raju\",\"sender_id\":20,\"receiver_id\":28}', '1', '2020-11-18 01:44:08', '2020-12-15 09:12:58', NULL),
(7, 20, 28, 'Order Accepted', 'Your order accepted by raju  singh', '{\"key\":\"Order_Accepted\",\"title\":\"Order Accepted\",\"message\":\"Your order accepted by raju  singh\",\"sender_id\":20,\"receiver_id\":28}', '1', '2020-11-25 01:18:41', '2020-12-15 09:12:58', NULL),
(8, 20, 28, 'Order Accepted', 'Your order accepted by raju  singh', '{\"key\":\"Order_Accepted\",\"title\":\"Order Accepted\",\"message\":\"Your order accepted by raju  singh\",\"sender_id\":20,\"receiver_id\":28}', '1', '2020-11-25 01:18:47', '2020-12-15 09:12:58', NULL),
(9, 20, 28, 'Order Accepted', 'Your order accepted by raju  singh', '{\"key\":\"Order_Accepted\",\"title\":\"Order Accepted\",\"message\":\"Your order accepted by raju  singh\",\"sender_id\":20,\"receiver_id\":28}', '1', '2020-11-25 01:18:53', '2020-12-15 09:12:58', NULL),
(10, 20, 28, 'Order Completed', 'Your order completed by raju  singh', '{\"key\":\"Order_Completed\",\"title\":\"Order Completed\",\"message\":\"Your order completed by raju  singh\",\"sender_id\":20,\"receiver_id\":28}', '1', '2020-11-25 01:25:28', '2020-12-15 09:12:58', NULL),
(11, 31, 13, 'Order Accepted', 'Your order accepted by Beauty Center', '{\"key\":\"Order_Accepted\",\"title\":\"Order Accepted\",\"message\":\"Your order accepted by Beauty Center\",\"sender_id\":31,\"receiver_id\":13}', '0', '2020-11-25 21:23:19', '2020-11-25 21:23:19', NULL),
(12, 31, 13, 'Order Accepted', 'Your order accepted by Beauty Center', '{\"key\":\"Order_Accepted\",\"title\":\"Order Accepted\",\"message\":\"Your order accepted by Beauty Center\",\"sender_id\":31,\"receiver_id\":13}', '0', '2020-11-25 21:23:34', '2020-11-25 21:23:34', NULL),
(13, 31, 13, 'Order Rejected', 'Your order rejected by Beauty Center', '{\"key\":\"Order_Rejected\",\"title\":\"Order Rejected\",\"message\":\"Your order rejected by Beauty Center\",\"sender_id\":31,\"receiver_id\":13}', '0', '2020-11-25 21:23:41', '2020-11-25 21:23:41', NULL),
(14, 31, 13, 'Order Rejected', 'Your order rejected by Beauty Center', '{\"key\":\"Order_Rejected\",\"title\":\"Order Rejected\",\"message\":\"Your order rejected by Beauty Center\",\"sender_id\":31,\"receiver_id\":13}', '0', '2020-11-25 21:25:22', '2020-11-25 21:25:22', NULL),
(15, 31, 13, 'Order Accepted', 'Your order accepted by Beauty Center', '{\"key\":\"Order_Accepted\",\"title\":\"Order Accepted\",\"message\":\"Your order accepted by Beauty Center\",\"sender_id\":31,\"receiver_id\":13}', '0', '2020-11-25 21:47:32', '2020-11-25 21:47:32', NULL),
(16, 34, 35, 'Order Accepted', 'Your order accepted by Buety 123', '{\"key\":\"Order_Accepted\",\"title\":\"Order Accepted\",\"message\":\"Your order accepted by Buety 123\",\"sender_id\":34,\"receiver_id\":35}', '1', '2020-11-26 08:34:53', '2020-11-26 08:36:13', NULL),
(17, 34, 35, 'Order Accepted', 'Your order accepted by Buety 123', '{\"key\":\"Order_Accepted\",\"title\":\"Order Accepted\",\"message\":\"Your order accepted by Buety 123\",\"sender_id\":34,\"receiver_id\":35}', '1', '2020-11-26 08:35:02', '2020-11-26 08:36:13', NULL),
(18, 34, 35, 'Order Accepted', 'Your order accepted by Buety 123', '{\"key\":\"Order_Accepted\",\"title\":\"Order Accepted\",\"message\":\"Your order accepted by Buety 123\",\"sender_id\":34,\"receiver_id\":35}', '1', '2020-11-26 08:35:16', '2020-11-26 08:36:13', NULL),
(19, 34, 35, 'Order Accepted', 'Your order accepted by Buety 123', '{\"key\":\"Order_Accepted\",\"title\":\"Order Accepted\",\"message\":\"Your order accepted by Buety 123\",\"sender_id\":34,\"receiver_id\":35}', '1', '2020-11-26 08:35:22', '2020-11-26 08:36:13', NULL),
(20, 20, 35, 'Order Accepted', 'Your order accepted by raju  singh', '{\"key\":\"Order_Accepted\",\"title\":\"Order Accepted\",\"message\":\"Your order accepted by raju  singh\",\"sender_id\":20,\"receiver_id\":35}', '0', '2020-11-26 09:43:06', '2020-11-26 09:43:06', NULL),
(21, 20, 28, 'Order Accepted', 'Your order accepted by raju  singh', '{\"key\":\"Order_Accepted\",\"title\":\"Order Accepted\",\"message\":\"Your order accepted by raju  singh\",\"sender_id\":20,\"receiver_id\":28}', '1', '2020-11-27 04:26:25', '2020-12-15 09:12:58', NULL),
(22, 31, 13, 'Order Accepted', 'Your order accepted by Beauty Center', '{\"key\":\"Order_Accepted\",\"title\":\"Order Accepted\",\"message\":\"Your order accepted by Beauty Center\",\"sender_id\":31,\"receiver_id\":13}', '0', '2020-11-27 11:20:40', '2020-11-27 11:20:40', NULL),
(23, 31, 13, 'Order Accepted', 'Your order accepted by Beauty Center', '{\"key\":\"Order_Accepted\",\"title\":\"Order Accepted\",\"message\":\"Your order accepted by Beauty Center\",\"sender_id\":31,\"receiver_id\":13}', '0', '2020-11-27 13:21:46', '2020-11-27 13:21:46', NULL),
(24, 20, 10, 'Order Accepted', 'Your order accepted by raju  singh', '{\"key\":\"Order_Accepted\",\"title\":\"Order Accepted\",\"message\":\"Your order accepted by raju  singh\",\"sender_id\":20,\"receiver_id\":10}', '0', '2020-12-07 01:18:01', '2020-12-07 01:18:01', NULL),
(25, 20, 10, 'Order Completed', 'Your order completed by raju  singh', '{\"key\":\"Order_Completed\",\"title\":\"Order Completed\",\"message\":\"Your order completed by raju  singh\",\"sender_id\":20,\"receiver_id\":10}', '0', '2020-12-07 01:18:43', '2020-12-07 01:18:43', NULL),
(26, 20, 10, 'Order Accepted', 'Your order accepted by raju  singh', '{\"key\":\"Order_Accepted\",\"title\":\"Order Accepted\",\"message\":\"Your order accepted by raju  singh\",\"sender_id\":20,\"receiver_id\":10}', '0', '2020-12-07 01:25:09', '2020-12-07 01:25:09', NULL),
(27, 20, 10, 'Order Accepted', 'Your order accepted by raju  singh', '{\"key\":\"Order_Accepted\",\"title\":\"Order Accepted\",\"message\":\"Your order accepted by raju  singh\",\"sender_id\":20,\"receiver_id\":10}', '0', '2020-12-07 01:28:20', '2020-12-07 01:28:20', NULL),
(28, 20, 10, 'Order Completed', 'Your order completed by raju  singh', '{\"key\":\"Order_Completed\",\"title\":\"Order Completed\",\"message\":\"Your order completed by raju  singh\",\"sender_id\":20,\"receiver_id\":10}', '0', '2020-12-07 01:29:30', '2020-12-07 01:29:30', NULL),
(29, 20, 10, 'Order Completed', 'Your order completed by raju  singh', '{\"key\":\"Order_Completed\",\"title\":\"Order Completed\",\"message\":\"Your order completed by raju  singh\",\"sender_id\":20,\"receiver_id\":10}', '0', '2020-12-07 01:31:02', '2020-12-07 01:31:02', NULL),
(30, 20, 10, 'Order Rejected', 'Your order rejected by raju  singh', '{\"key\":\"Order_Rejected\",\"title\":\"Order Rejected\",\"message\":\"Your order rejected by raju  singh\",\"sender_id\":20,\"receiver_id\":10}', '0', '2020-12-07 01:31:59', '2020-12-07 01:31:59', NULL),
(31, 20, 10, 'Order Accepted', 'Your order accepted by raju  singh', '{\"key\":\"Order_Accepted\",\"title\":\"Order Accepted\",\"message\":\"Your order accepted by raju  singh\",\"sender_id\":20,\"receiver_id\":10}', '0', '2020-12-08 02:12:21', '2020-12-08 02:12:21', NULL),
(32, 20, 28, 'Order Accepted', 'Your order accepted by raju  singh', '{\"key\":\"Order_Accepted\",\"title\":\"Order Accepted\",\"message\":\"Your order accepted by raju  singh\",\"sender_id\":20,\"receiver_id\":28}', '1', '2020-12-11 08:38:33', '2020-12-15 09:12:58', NULL),
(33, 20, 28, 'Order Accepted', 'Your order accepted by raju  singh', '{\"key\":\"Order_Accepted\",\"title\":\"Order Accepted\",\"message\":\"Your order accepted by raju  singh\",\"sender_id\":20,\"receiver_id\":28}', '1', '2020-12-11 08:39:05', '2020-12-15 09:12:58', NULL),
(34, 20, 28, 'Order Accepted', 'Your order accepted by raju  singh', '{\"key\":\"Order_Accepted\",\"title\":\"Order Accepted\",\"message\":\"Your order accepted by raju  singh\",\"sender_id\":20,\"receiver_id\":28}', '1', '2020-12-11 08:40:09', '2020-12-15 09:12:58', NULL),
(35, 20, 28, 'Order Accepted', 'Your order accepted by raju  singh', '{\"key\":\"Order_Accepted\",\"title\":\"Order Accepted\",\"message\":\"Your order accepted by raju  singh\",\"sender_id\":20,\"receiver_id\":28}', '1', '2020-12-11 08:44:13', '2020-12-15 09:12:58', NULL),
(36, 20, 28, 'Order Accepted', 'Your order accepted by raju  singh', '{\"key\":\"Order_Accepted\",\"title\":\"Order Accepted\",\"message\":\"Your order accepted by raju  singh\",\"sender_id\":20,\"receiver_id\":28}', '1', '2020-12-11 08:44:22', '2020-12-15 09:12:58', NULL),
(37, 20, 28, 'Order Accepted', 'Your order accepted by raju  singh', '{\"key\":\"Order_Accepted\",\"title\":\"Order Accepted\",\"message\":\"Your order accepted by raju  singh\",\"sender_id\":20,\"receiver_id\":28}', '1', '2020-12-11 08:44:46', '2020-12-15 09:12:58', NULL),
(38, 20, 28, 'Order Accepted', 'Your order accepted by raju  singh', '{\"key\":\"Order_Accepted\",\"title\":\"Order Accepted\",\"message\":\"Your order accepted by raju  singh\",\"sender_id\":20,\"receiver_id\":28}', '1', '2020-12-11 08:44:56', '2020-12-15 09:12:58', NULL),
(39, 20, 28, 'Order Accepted', 'Your order accepted by raju  singh', '{\"key\":\"Order_Accepted\",\"title\":\"Order Accepted\",\"message\":\"Your order accepted by raju  singh\",\"sender_id\":20,\"receiver_id\":28}', '1', '2020-12-11 08:45:06', '2020-12-15 09:12:58', NULL),
(40, 20, 28, 'Order Accepted', 'Your order accepted by raju  singh', '{\"key\":\"Order_Accepted\",\"title\":\"Order Accepted\",\"message\":\"Your order accepted by raju  singh\",\"sender_id\":20,\"receiver_id\":28}', '1', '2020-12-11 08:45:32', '2020-12-15 09:12:58', NULL),
(41, 20, 28, 'Order Accepted', 'Your order accepted by raju  singh', '{\"key\":\"Order_Accepted\",\"title\":\"Order Accepted\",\"message\":\"Your order accepted by raju  singh\",\"sender_id\":20,\"receiver_id\":28}', '1', '2020-12-11 09:01:01', '2020-12-15 09:12:58', NULL),
(42, 20, 28, 'Order Accepted', 'Your order accepted by raju  singh', '{\"key\":\"Order_Accepted\",\"title\":\"Order Accepted\",\"message\":\"Your order accepted by raju  singh\",\"sender_id\":20,\"receiver_id\":28}', '1', '2020-12-11 09:01:16', '2020-12-15 09:12:58', NULL),
(43, 20, 28, 'Order Accepted', 'Your order accepted by raju  singh', '{\"key\":\"Order_Accepted\",\"title\":\"Order Accepted\",\"message\":\"Your order accepted by raju  singh\",\"sender_id\":20,\"receiver_id\":28}', '1', '2020-12-11 09:22:59', '2020-12-15 09:12:58', NULL),
(44, 20, 6, 'Order Accepted', 'Your order accepted by raju  singh', '{\"key\":\"Order_Accepted\",\"title\":\"Order Accepted\",\"message\":\"Your order accepted by raju  singh\",\"sender_id\":20,\"receiver_id\":6}', '1', '2020-12-14 05:31:05', '2020-12-14 05:33:01', NULL),
(45, 20, 6, 'Order Completed', 'Your order completed by raju  singh', '{\"key\":\"Order_Completed\",\"title\":\"Order Completed\",\"message\":\"Your order completed by raju  singh\",\"sender_id\":20,\"receiver_id\":6}', '1', '2020-12-14 05:32:11', '2020-12-14 05:33:01', NULL),
(46, 40, 39, 'Payment Successfully', 'Your payment successfully by Inspiring', '{\"key\":\"Payment_Success\",\"title\":\"Payment Successfully\",\"message\":\"Your payment successfully by Inspiring\",\"sender_id\":40,\"receiver_id\":39}', '0', '2020-12-14 08:26:11', '2020-12-14 08:26:11', NULL),
(47, 40, 39, 'New booking', 'You have a new booking by Inspiring', '{\"key\":\"Payment_Success\",\"title\":\"New booking\",\"message\":\"You have a new booking by Inspiring\",\"sender_id\":40,\"receiver_id\":39}', '0', '2020-12-14 08:33:35', '2020-12-14 08:33:35', NULL),
(48, 39, 40, 'Order Accepted', 'Your order accepted by Codemeg Soft Solution', '{\"key\":\"Order_Accepted\",\"title\":\"Order Accepted\",\"message\":\"Your order accepted by Codemeg Soft Solution\",\"sender_id\":39,\"receiver_id\":40}', '0', '2020-12-14 08:35:09', '2020-12-14 08:35:09', NULL),
(49, 39, 40, 'Order Rejected', 'Your order rejected by Codemeg Soft Solution', '{\"key\":\"Order_Rejected\",\"title\":\"Order Rejected\",\"message\":\"Your order rejected by Codemeg Soft Solution\",\"sender_id\":39,\"receiver_id\":40}', '0', '2020-12-14 08:35:31', '2020-12-14 08:35:31', NULL),
(50, 39, 40, 'Order Accepted', 'Your order accepted by Codemeg Soft Solution', '{\"key\":\"Order_Accepted\",\"title\":\"Order Accepted\",\"message\":\"Your order accepted by Codemeg Soft Solution\",\"sender_id\":39,\"receiver_id\":40}', '0', '2020-12-14 08:35:39', '2020-12-14 08:35:39', NULL),
(51, 40, 39, 'New booking', 'You have a new booking by Inspiring', '{\"key\":\"new_order\",\"title\":\"New booking\",\"message\":\"You have a new booking by Inspiring\",\"sender_id\":40,\"receiver_id\":39}', '0', '2020-12-14 08:39:36', '2020-12-14 08:39:36', NULL),
(52, 39, 40, 'Order Accepted', 'Your order accepted by Codemeg Soft Solution', '{\"key\":\"Order_Accepted\",\"title\":\"Order Accepted\",\"message\":\"Your order accepted by Codemeg Soft Solution\",\"sender_id\":39,\"receiver_id\":40}', '0', '2020-12-14 08:39:50', '2020-12-14 08:39:50', NULL),
(53, 28, 20, 'New booking', 'You have a new booking by ramlal', '{\"key\":\"new_order\",\"title\":\"New booking\",\"message\":\"You have a new booking by ramlal\",\"sender_id\":28,\"receiver_id\":20}', '1', '2020-12-15 09:11:31', '2020-12-18 02:14:22', NULL),
(54, 20, 28, 'Booking Accepted', 'Your booking accepted by raju  singh', '{\"key\":\"Order_Accepted\",\"title\":\"Booking Accepted\",\"message\":\"Your booking accepted by raju  singh\",\"sender_id\":20,\"receiver_id\":28}', '1', '2020-12-15 09:12:45', '2020-12-15 09:12:58', NULL),
(55, 42, 41, 'New booking', 'You have a new booking by Jhgfdsa', '{\"key\":\"new_order\",\"title\":\"New booking\",\"message\":\"You have a new booking by Jhgfdsa\",\"sender_id\":42,\"receiver_id\":41}', '1', '2020-12-15 15:36:15', '2020-12-16 06:14:13', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `page_id` int(11) NOT NULL,
  `type` enum('1','2') NOT NULL COMMENT '1==about_us,2==terms&conditions',
  `content` longtext NOT NULL,
  `content_es` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`page_id`, `type`, `content`, `content_es`) VALUES
(1, '1', '<div class=\"body\" style=\"padding: 10px 15px;\">\r\n	<h3 class=\"section-heading\" style=\"text-align: center; font-size: 22px;\">Privacy Policy</h3>\r\n\r\n	<!-------------------Englis pp--------------------->\r\n	<h4 class=\"sub-heading french\">B&A GROUP built the YoPO a Commercial app. This SERVICE is provided by B&A GROUP and is intended for use as is.</h4>\r\n\r\n	<p>This page is used to inform visitors regarding our policies with the collection, use, and disclosure of Personal Information if anyone decided to use our Service.</p>\r\n\r\n	<p>If you choose to use our Service, then you agree to the collection and use of information in relation to this policy. The Personal Information that we collect is used for providing and improving the Service. We will not use or share your information with anyone except as described in this Privacy Policy.</p>\r\n\r\n	<p>The terms used in this Privacy Policy have the same meanings as in our Terms and Conditions, which is accessible at YoPo unless otherwise defined in this Privacy Policy.</p>\r\n\r\n\r\n\r\n	<h4 class=\"sub-heading\">Information Collection and Use</h4>\r\n	<p>For a better experience, while using our Service, we may require you to provide us with certain personally identifiable information, including but not limited to Username, Password, adrees, social security number or tax id number  Mobile number. id witth picture, and one selfie. The information that we request will be retained by us and used as described in this privacy policy.</p>\r\n\r\n	<p>The app does use third party services that may collect information used to identify you.</p>\r\n\r\n	<p>Link to privacy policy of third party service providers used by the app</p>\r\n\r\n	<p>Payment will be charged to iTunes Account at confirmation of purchase</p>\r\n\r\n	<p>Subscription automatically renews unless auto-renew is turned off at least 24-hours before the end of the current period\r\n	Account will be charged for renewal within 24-hours prior to the end of the current period, and identify the cost of the renewal\r\n	Subscriptions may be managed by the user and auto-renewal may be turned off by going to the user\'s Account Settings after purchase</p>\r\n	\r\n	<h4 class=\"sub-heading\">Log Data</h4>\r\n	<p>We want to inform you that whenever you use our Service, in a case of an error in the app we collect data and information (through third party products) on your phone called Log Data. This Log Data may include information such as your device Internet Protocol (“IP”) address, device name, operating system version, the configuration of the app when utilizing our Service, the time and date of your use of the Service, and other statistics.</p>\r\n\r\n	<h4 class=\"sub-heading\">Cookies</h4>\r\n\r\n	<p>Cookies are files with a small amount of data that are commonly used as anonymous unique identifiers. These are sent to your browser from the websites that you visit and are stored on your device\'s internal memory.</p>\r\n\r\n	<p>This Service does not use these “cookies” explicitly. However, the app may use third party code and libraries that use “cookies” to collect information and improve their services. You have the option to either accept or refuse these cookies and know when a cookie is being sent to your device. If you choose to refuse our cookies, you may not be able to use some portions of this Service.</p>\r\n\r\n	<h4 class=\"sub-heading\">Service Providers</h4>\r\n\r\n	<p>We may employ third-party</p>\r\n\r\n	<p>companies and individuals due to the following reasons:</p>\r\n\r\n	<ul>\r\n		<li>To provide the Service on our behalf;</li>\r\n		<li>To perform Service-related services; or</li>\r\n		<li>To assist us in analyzing how our Service is used.</li>\r\n		<li>We want to inform users of this Service that these third parties have access to your Personal Information. The reason is to perform the tasks assigned to them on our behalf. However, they are obligated not to disclose or use the information for any other purpose.</li>\r\n	</ul>\r\n\r\n	<h4 class=\"sub-heading\">Security</h4>\r\n\r\n	<p>We value your trust in providing us your Personal Information, thus we are striving to use commercially acceptable means of protecting it. But remember that no method of transmission over the internet, or method of electronic storage is 100% secure and reliable, and we cannot guarantee its absolute security.</p>\r\n\r\n	<h4 class=\"sub-heading\">Links to Other Sites</h4>\r\n	<p>This Service may contain links to other sites. If you click on a third-party link, you will be directed to that site. Note that these external sites are not operated by us. Therefore, we strongly advise you to review the Privacy Policy of these websites. We have no control over and assume no responsibility for the content, privacy policies, or practices of any third-party sites or services.</p>\r\n\r\n	<h4 class=\"sub-heading\">Children’s Privacy</h4>\r\n	<p>These Services do not address anyone under the age of 18. We do not knowingly collect personally identifiable information from children under 18. In the case we discover that a child under 13 has provided us with personal information, we immediately delete this from our servers. If you are a parent or guardian and you are aware that your child has provided us with personal information, please contact us so that we will be able to do necessary actions.</p>\r\n\r\n	<h4 class=\"sub-heading\">Changes to This Privacy Policy</h4>\r\n	<p>We may update our Privacy Policy from time to time. Thus, you are advised to review this page periodically for any changes. We will notify you of any changes by posting the new Privacy Policy on this page. These changes are effective immediately after they are posted on this page.</p>\r\n\r\n	<h4 class=\"sub-heading\">Contact Us</h4>\r\n	<p>If you have any questions or suggestions about our Privacy Policy, do not hesitate to contact us at <a href=\"mailto:contact@yopo.us\">contact@yopo.us</a></p>\r\n</div>', '<div class=\"body\" style=\"padding: 10px 15px;\">\r\n	<h3 class=\"section-heading\" style=\"text-align: center; font-size: 22px;\">Política de privacidad</h3>\r\n\r\n	<!-------------------french pp--------------------->\r\n	<h4 class=\"sub-heading french\">B&A GROUP creó YoPO, una aplicación comercial. Este SERVICIO es proporcionado por B&A GROUP y está diseñado para usarse tal cual.</h4>\r\n\r\n	<p>Esta página se utiliza para informar a los visitantes sobre nuestras políticas con la recopilación, uso y divulgación de información personal si alguien decide utilizar nuestro servicio.</p>\r\n\r\n	<p>Si elige utilizar nuestro Servicio, entonces acepta la recopilación y el uso de información en relación con esta política. La información personal que recopilamos se utiliza para proporcionar y mejorar el Servicio. No utilizaremos ni compartiremos su información con nadie, excepto como se describe en esta Política de privacidad.</p>\r\n\r\n	<p>Los términos utilizados en esta Política de privacidad tienen el mismo significado que en nuestros Términos y condiciones, a los que se puede acceder en YoPo a menos que se defina lo contrario en esta Política de privacidad.</p>\r\n\r\n\r\n\r\n	<h4 class=\"sub-heading\">Recolección de información y uso</h4>\r\n\r\n	<p>Para una mejor experiencia, mientras utiliza nuestro Servicio, es posible que le solicitemos que nos brinde cierta información de identificación personal, que incluye, entre otros, Nombre de usuario, Contraseña, destinatarios, número de seguro social o número de identificación fiscal Número de teléfono móvil. Identificación con foto y una selfie. La información que solicitamos será retenida por nosotros y utilizada como se describe en esta política de privacidad.</p>\r\n\r\n	<p>La aplicación utiliza servicios de terceros que pueden recopilar información utilizada para identificarlo.</p>\r\n\r\n	<p>Enlace a la política de privacidad de terceros proveedores de servicios utilizados por la aplicación</p>\r\n\r\n	<p>Suscripción Auto Renovable</p>\r\n\r\n	<p>El pago se cargará a la cuenta de iTunes en la confirmación de la compra.</p>\r\n\r\n	<p>La suscripción se renueva automáticamente a menos que la renovación automática esté desactivada al menos 24 horas antes del final del período actual\r\n	Se le cobrará a la cuenta por la renovación dentro de las 24 horas anteriores al final del período actual e identificará el costo de la renovación\r\n	Las suscripciones pueden ser administradas por el usuario y la renovación automática puede desactivarse yendo a la Configuración de la cuenta del usuario después de la compra</p>\r\n	\r\n	<h4 class=\"sub-heading\">Dato de registro</h4>\r\n	<p>Queremos informarle que cada vez que utiliza nuestro Servicio, en caso de un error en la aplicación, recopilamos datos e información (a través de productos de terceros) en su teléfono llamados Datos de registro. Estos Datos de registro pueden incluir información como la dirección del Protocolo de Internet (\"IP\") de su dispositivo, el nombre del dispositivo, la versión del sistema operativo, la configuración de la aplicación cuando utiliza nuestro Servicio, la hora y la fecha de uso del Servicio y otras estadísticas.</p>\r\n\r\n	<h4 class=\"sub-heading\">Galletas</h4>\r\n\r\n	<p>Las cookies son archivos con una pequeña cantidad de datos que se usan comúnmente como identificadores únicos anónimos. Estos se envían a su navegador desde los sitios web que visita y se almacenan en la memoria interna de su dispositivo.</p>\r\n\r\n	<p>Este servicio no utiliza estas \"cookies\" explícitamente. Sin embargo, la aplicación puede usar código de terceros y bibliotecas que usan \"cookies\" para recopilar información y mejorar sus servicios. Tiene la opción de aceptar o rechazar estas cookies y saber cuándo se envía una cookie a su dispositivo. Si elige rechazar nuestras cookies, es posible que no pueda usar algunas partes de este Servicio.</p>\r\n\r\n	<h4 class=\"sub-heading\">Proveedores de servicio</h4>\r\n\r\n	<p>Podemos emplear a terceros</p>\r\n\r\n	<p>empresas y particulares por los siguientes motivos:</p>\r\n\r\n	<ul>\r\n		<li>Para facilitar nuestro servicio;</li>\r\n		<li>Para proporcionar el Servicio en nuestro nombre;</li>\r\n		<li>Para realizar servicios relacionados con el Servicio; o</li>\r\n		<li>Para ayudarnos a analizar cómo se utiliza nuestro Servicio.</li>\r\n		<li>Queremos informar a los usuarios de este Servicio que estos terceros tienen acceso a su Información personal. La razón es realizar las tareas que se les asignaron en nuestro nombre. Sin embargo, están obligados a no divulgar ni utilizar la información para ningún otro propósito.</li>\r\n	</ul>\r\n\r\n	<h4 class=\"sub-heading\">Seguridad</h4>\r\n\r\n	<p>Valoramos su confianza en proporcionarnos su información personal, por lo tanto, nos esforzamos por utilizar medios comercialmente aceptables para protegerla. Pero recuerde que ningún método de transmisión por Internet o método de almacenamiento electrónico es 100% seguro y confiable, y no podemos garantizar su seguridad absoluta.</p>\r\n\r\n	<h4 class=\"sub-heading\">Enlaces a otros sitios</h4>\r\n\r\n	<p>Este servicio puede contener enlaces a otros sitios. Si hace clic en un enlace de un tercero, será dirigido a ese sitio. Tenga en cuenta que estos sitios externos no son operados por nosotros. Por lo tanto, le recomendamos encarecidamente que revise la Política de privacidad de estos sitios web. No tenemos control ni asumimos ninguna responsabilidad por el contenido, las políticas de privacidad o las prácticas de sitios o servicios de terceros.</p>\r\n\r\n	<h4 class=\"sub-heading\">Privacidad de los niños</h4>\r\n	<p>Estos servicios no se dirigen a ninguna persona menor de 18 años. No recopilamos a sabiendas información de identificación personal de niños menores de 18 años. En el caso de que descubramos que un niño menor de 13 años nos ha proporcionado información personal, la eliminamos de inmediato de nuestros servidores. Si usted es padre o tutor y sabe que su hijo nos ha proporcionado información personal, contáctenos para que podamos realizar las acciones necesarias.</p>\r\n\r\n	<h4 class=\"sub-heading\">Cambios a esta política de privacidad</h4>\r\n	<p>Podemos actualizar nuestra Política de privacidad de vez en cuando. Por lo tanto, se recomienda que revise esta página periódicamente en busca de cambios. Le notificaremos cualquier cambio publicando la nueva Política de privacidad en esta página. Estos cambios son efectivos inmediatamente después de su publicación en esta página.</p>\r\n\r\n	<h4 class=\"sub-heading\">Contact Us</h4>\r\n	<p>Si tiene alguna pregunta o sugerencia sobre nuestra Política de privacidad, no dude en contactarnos a <a href=\"mailto:contact@yopo.us\">contact@yopo.us</a></p>\r\n</div>');
INSERT INTO `pages` (`page_id`, `type`, `content`, `content_es`) VALUES
(2, '2', '<div class=\"body\" style=\"padding: 10px 15px;\">\r\n	<h3 class=\"section-heading\" style=\"text-align: center; font-size: 22px;\">TERMS AND CONDITIONS</h3>\r\n\r\n	<h4>TERMS OF USE</h4>\r\n	<p>By downloading, browsing, accessing or using this mobile application (“YoPO”), you agree to be bound by these Terms and Conditions of Use. We reserve the right to amend these terms and conditions at any time. If you disagree with any of these Terms and Conditions of Use, you must immediately discontinue your access to the Mobile Application and your use of the services offered on the Mobile Application. Continued use of the Mobile Application will constitute acceptance of these Terms and Conditions of Use, as may be amended from time to time.</p>\r\n\r\n	<h4>DEFINITIONS</h4>\r\n	<p>In these Terms and Conditions of Use, the following capitalised terms shall have the following meanings, except where the context otherwise requires: \"Account\" means an account created by a User on the Mobile Application as part of Registration. \"Privacy Policy\" means the privacy policy set out in Clause 14 of these Terms and Conditions of Use.\"Register\" means to create an Account on the Mobile Application and \"Registration\" means the act of creating such an Account. \"Services\" means all the services provided by YoPo  via the Mobile Application to Users, and \"Service\" means any one of them, \"Users\" means users of the Mobile Application, including you and \"User\" means any one of them.</p>\r\n\r\n	<h4>GENERAL ISSUES ON MOBILE APPLICATION AND SERVICES</h4>\r\n	<p>Applicability of the terms and conditions: the use of the Services and / or the Mobile Application are subject to these Terms and conditions of use.</p>\r\n	<p>Location: the mobile application, services and exchanges are intended exclusively for users who access the mobile application. We do not guarantee that the Services are available or are suitable for use, nor are we responsible for any unwanted activity as a product of the use of the app, if you access the Mobile Application, use the Services is responsible for the consequences and compliance with All applicable laws.</p>\r\n	<p>YoPo is not an employment agency for which we are not responsible for safeguarding a service provider while performing a service, nor is it safeguarding the user who hires a service, it is the responsibility of each party involved for their own security, we are only a platform for service connection.</p>\r\n	<p>Scope: the mobile application, the services are for your personal non-commercial use and should not be used for commercial purposes.</p>\r\n	<p>Prevention in use: We reserve the right to prevent you from using the Mobile Application and prevent misuse</p>\r\n	<p>Equipment and networks: the provision of the Services and the Mobile Application requires the use of a mobile phone necessary to access the Mobile Application, you will need Internet connectivity and adequate telecommunications links. You acknowledge that the terms of the agreement with your respective mobile network provider will remain in effect when you use the Mobile Application. As a result, the Mobile Service Provider may charge you for access to network connection services for the duration of the connection while accessing the Mobile Application or third-party charges that may arise. You accept responsibility for any charges that arise.</p>\r\n	<p>Permission to use the mobile application: If you are not the bill payer of the mobile phone or portable device that is used to access the mobile application, you will be assumed to have received the bill payer\'s permission to use the mobile application.</p>\r\n	<p>License to use the material: by sending any text or image (including photographs) (\"Material\") through the Application, you declare that you are the owner of the Material or that you have the proper authorization of the owner of the Material to use, reproduce and distribute it You hereby grant us a worldwide, royalty-free and non-exclusive license to use the Material to promote any product or service</p>\r\n\r\n	<h4>PAYMENTS AND COMMISSIONS</h4>\r\n	<p>Both the provider and the user are aware that they must pay a service fee according to the service performed, the quality may vary over time and that it will be known to both parties when contracting a service on the user\'s side or Offer a service on the seller side.</p>\r\n	<p>additionally on the provider side of your payment after discounted yopo commissions, the PayPal transfer commission will be deducted, which is the payment platform used by our application to send the money to the provider and recover the money from the user . These commissions are those listed by PayPal on their platform.</p>\r\n	<p>which is currently 2.9% of the value of the transfer plus 0.30 dollar if it is national 0 4.9% of the value of the transfer plus 0.30 dollar if it is international, these values may vary according to PayPal policies without this incurring responsibility for YoPo</p>\r\n\r\n	<h4>LOCATION ALERTS AND NOTIFICATIONS</h4>\r\n	<p>You agree to receive pre-programmed notifications (“Location Alerts”) on the Mobile Application from Merchants if you have turned on locational services on your mobile telephone or other handheld devices (as the case may be).</p>\r\n\r\n	<h4>YOUR OBLIGATIONS</h4>\r\n	<ul>\r\n		<li>User terms: You agree (and must comply with) the terms and conditions of the user, which may be modified from time to time.</li>\r\n		<li>Accurate information: You guarantee that all information provided in the Registry and contained as part of your Account is true, complete and accurate and that you will immediately inform us of any changes in said information by updating the information in your Account.</li>\r\n		<li>Content in the Mobile Application and the Service: It is your responsibility to ensure that any product or information available through the Mobile Application or the Services meets your specific requirements before performing any service.</li>\r\n		<li>Prohibitions regarding the use of the Services or the Mobile Application: without limitation, you agree not to use or allow another person to use the Services or the Mobile Application:</li>\r\n		<li>to send or receive any service that is not civil or in good taste</li>\r\n		<li>to send or receive any service that is threatening, seriously offensive, indecent, obscene or threatening, blasphemous or defamatory of any person, in contempt of court or in violation of trust, copyright, personality rights, publicity or privacy or any other third party rights;</li>\r\n		<li>to send or receive any service that encourages conduct that would be considered a criminal offense, would result in civil liability, or that would be contrary to the law or infringe the rights of a third party in any country in the world;</li>\r\n		<li>to send or receive any service that is technically harmful or</li>\r\n		<li>for a purpose other than that we design or intend to use;</li>\r\n		<li>for any fraudulent purpose;</li>\r\n		<li>apart from complying with accepted Internet practices and practices of any connected network;</li>\r\n		<li>in any way that is calculated to incite hatred against any ethnic, religious or other minority, or that is adversely calculated to affect any individual, group or entity; or</li>\r\n		<li>such that, or commits any act that imposes or imposes, an unreasonable or disproportionately large burden on our infrastructure.</li>\r\n		<li>Prohibitions in relation to the use of the Services, the Mobile Application: without limitation, you agree not to allow anyone else to:</li>\r\n		<li>resell any service;</li>\r\n		<li>provide false information, including false names, addresses and contact information, and fraudulently use credit / debit card numbers;</li>\r\n		<li></li>\r\n		<li>try to bypass our security or network, including access to data that is not intended for you, log in to a server or account that is not expressly authorized to access or test the security of other networks (such as running a port scan) ;</li>\r\n		<li>execute any form of network monitoring that will intercept data not intended for you;</li>\r\n		<li>engage in fraudulent interactions or transactions with us or with a Merchant (including the interaction or transaction allegedly on behalf of a third party in which you have no authority to bind that third party or if you intend to be a third party);</li>\r\n		<li>extract data or hack the mobile application;</li>\r\n		<li>use the Services or the Mobile Application in breach of these Terms and conditions of use;</li>\r\n		<li>participate in any illegal activity related to the use of the Mobile Application or the Services; or</li>\r\n		<li>Participate in any conduct that, in our reasonable and exclusive opinion, restricts or inhibits any other customer from using or properly enjoying the Application or Mobile Services</li>\r\n	</ul>\r\n\r\n	<h4>RULES ON THE USE OF THE SERVICE AND THE MOBILE APPLICATION</h4>\r\n		<p>We will make every reasonable effort to correct any errors or omissions as soon as possible after being notified of them. However, we do not guarantee that the Services or the Mobile Application are free from failures, and we accept no responsibility for such failures, errors or omissions. In case of any error, failure or omission, you must report it by contacting us at 18018880954.</p>\r\n		<p>We do not guarantee that your use of the Services or the Mobile Application will be uninterrupted and we do not guarantee that any information (or message) transmitted through the Services or the Mobile Application will be transmitted accurately, reliably, in a timely manner or there. Although we will attempt to allow uninterrupted access to the Services and the Mobile Application, access to the Services and the Mobile Application may be suspended, restricted or canceled at any time.</p>\r\n		<p>We do not guarantee that the Services and the Mobile Application are free of viruses or anything else that may have a harmful effect on any technology.</p>\r\n		<p>We reserve the right to change, modify, replace, suspend or delete without prior notice any information or Services in the Mobile Application from time to time. Your access to the Mobile Application and / or the Services may also be occasionally restricted to allow repairs, maintenance or the introduction of new facilities or services. We will attempt to restore such access as soon as reasonably possible. For the avoidance of doubt, we reserve the right to withdraw any information or Services from the Mobile Application at any time.</p>\r\n		<p>We reserve the right to block access and / or edit or delete any material that, in our reasonable opinion, may result in a violation of these Terms and Conditions of Use.</p>\r\n\r\n		<h4>SUSPENSION AND TERMINATION</h4>\r\n		<p>If you use (or any person other than you, with your permission, use) the Mobile Application, any Service in violation of these Terms and Conditions of Use, we may suspend your use of the Services and / or the Mobile Application.</p>\r\n		<p>If we suspend the Services or the Mobile Application, we may refuse to restore the Services or the Mobile Application for use until we receive a guarantee from you, in a manner we deem acceptable, that there will be no further breach of the provisions of these Terms and conditions of use.</p>\r\n		<p>Without limitation to anything else in this Clause 8, we shall be entitled immediately or at any time (in whole or in part) to: (a) suspend the Services and / or the Mobile Application; (b) suspend your use of the Services and / or the Mobile Application; and / or (c) suspend the use of the Services and / or the Mobile Application for people we believe are connected (in any way) to you, if:</p>\r\n		<p>you commit a breach of these Terms and conditions of use;</p>\r\n		<p>We suspect, for reasonable reasons, that you have committed or could commit a violation of these Terms and Conditions of Use; or We suspect, for reasonable reasons, that you may have committed or committed fraud against us or anyone. Our rights under this Clause 8 will not prejudge any other rights or remedies we may have with respect to any breach or any right, obligation or liability accrued prior to termination.</p>\r\n\r\n		<h4>DISCLAIMER AND EXCLUSION OF LIABILITY</h4>\r\n		<p>The Mobile Application, the Services, information about the Mobile Application and the use of all related facilities are provided \"as is, as available\" without any warranty, either express or implied.</p>\r\n		<p>To the fullest extent permitted by applicable law, we waive all representations and warranties related to the Mobile Application and its content, including in connection with any inaccuracies or omissions in the Mobile Application, merchantability warranties, quality, suitability for a particular purpose. , accuracy, availability, no infringement or implied warranties of the course of negotiation or use of trade.</p>\r\n		<p>We do not guarantee that the mobile application is always accessible, uninterrupted, timely, secure, error-free or free of computer viruses or other invasive or harmful code or that the mobile application will not be affected by any act of God or other force majeure events, including the inability to obtain or the shortage of necessary materials, equipment, energy or telecommunications facilities, lack of telecommunications equipment or facilities and failure of information technology or telecommunications equipment or facilities.</p>\r\n		<p>While we can make reasonable efforts to include accurate and up-to-date information in the Mobile Application, we do not guarantee or represent its accuracy, timeliness or integrity.</p>\r\n		<p>We will not be reliable for any act or omission of third parties, regardless of their cause, or for direct, indirect, incidental, special, consequential or punitive damages, regardless of their cause, as a result of the mobile application and the services offered in the mobile application , your access, use or inability to use the mobile application or the services offered in the mobile application, the dependence or download of the mobile application and / or services, or any delay, inaccuracy in the information or its transmission, including, among others, damages for loss of business or profits, use, data or other intangibles, even if we have been informed of the possibility of such damages.</p>\r\n		<p>We will not be reliable in the contract, tort (including negligence or breach of legal duty) or in any other way and whatever the cause thereof, for any indirect, consequential, collateral, special or incidental loss or damage suffered or incurred by you. in relation to the Mobile Application and these Terms and conditions of use. For the purposes of these Terms and Conditions of Use, the indirect or consequential loss or damage includes, without limitation, loss of income, profits, anticipated savings or business, loss of data or goodwill, loss of use or value of any equipment, including software, third party claims, and all associated and incidental costs and expenses.</p>\r\n		<p>The foregoing exclusions and limitations apply only to the extent permitted by law. None of your legal rights as a consumer that cannot be excluded or limited is affected.</p>\r\n\r\n		<p>Despite our efforts to ensure that our system is secure, you acknowledge that all electronic data transfers are potentially susceptible to being intercepted by others. We cannot and do not guarantee that data transfers in accordance with the Mobile Application or the email transmitted to and from us will not be monitored or read by others.</p>\r\n\r\n		<h4>INDEMNITY</h4>\r\n		<p>You agree to indemnify and keep us indemnified against any claim, action, lawsuit or proceeding filed or threatened to be filed against us that is caused by (a) your use of the Services, (b) the use of any other part of the Services you use your user ID, verification PIN and / or any identification number assigned by YoPo, and / or (c) your breach of any of these Terms and Conditions of Use, and to pay us damages, costs and interests in connection with such claim , action, demand or procedure.</p>\r\n\r\n		<h4>INTELLECTUAL PROPERTY RIGHTS</h4>\r\n		<p>All editorial content, information, photographs, illustrations, graphic material and other graphic materials, and names, logos and trademarks registered in the Mobile Application are protected by copyright laws and / or other international laws and / or treaties, and we they belong and / or our suppliers, as the case may be. These works, logos, graphics, sounds or images may not be copied, reproduced, retransmitted, distributed, disseminated, sold, published, transmitted or distributed, either in whole or in part, unless expressly authorized by us and / or our suppliers, as case can be.</p>\r\n		<p>Nothing contained in the Mobile Application must be construed as a concession, by implication, impediment or otherwise, of any license or right of use of any trademark displayed in the Mobile Application without our written permission. The improper use of any trademark or any other content displayed in the Mobile Application is prohibited.</p>\r\n		<p>We will not hesitate to take legal action against the unauthorized use of our trademarks, names or symbols to preserve and protect your rights in the matter. All rights not expressly granted in this document are reserved. Other product and company names mentioned here may also be trademarks of their respective owners.</p>\r\n\r\n		<h4>AMENDMENTS</h4>\r\n		<p>We may periodically make changes to the content of the Mobile Application, including descriptions and prices of the goods and services advertised, at any time and without prior notice. We assume no responsibility for errors or omissions in the content of the Mobile Application.</p>\r\n		<p>We reserve the right to modify these Terms and Conditions from time to time without prior notice. The revised Terms and conditions of use will be published in the Mobile Application and will become effective as of the date of said publication. It is recommended to review these terms and conditions periodically, as they are binding on you.</p>\r\n\r\n		<h4>APPLICABLE LAW AND JURISDICTION</h4>\r\n		<p>The mobile application can be accessed from all countries of the world where local technology allows. As each of these places has different laws, when accessing the Mobile Application, both you and we accept that the laws of the United States, without taking into account the conflicts of legal principles, will apply to all matters related to the use of the Mobile app.</p>\r\n		<p>You agree and accept that both you and we will submit to the exclusive jurisdiction of the US courts with respect to any dispute that arises or is related to these Terms and Conditions of Use.</p>\r\n\r\n		<h4>Privacy Policy</h4>\r\n		<p>Access to the Mobile Application and the use of the Services offered in the Mobile Application by YoPo is subject to this Privacy Policy. By accessing the Mobile Application and continuing to use the Services offered, you are deemed to have accepted this Privacy Policy and, in particular, your consent has been given for us to use and disclose your personal information in the manner prescribed in this Privacy Policy and for the purposes set forth in Clauses 3.7. and / or 4.1. We reserve the right to modify this Privacy Policy from time to time. If you do not agree with any part of this Privacy Policy, you must immediately terminate your access to the Mobile Application and your use of the Services.</p>\r\n\r\n		<p>As part of the normal operation of our Services, we collect, use and, in some cases, disclose your information to third parties. Consequently, we have developed this Privacy Policy so that you can understand how we collect, use, communicate, disclose and make use of your personal information when you use the Services in the Mobile Application:</p>\r\n\r\n		<ol>\r\n			<li>Before or at the time of collecting personal information, we will identify the purposes for which the information is collected.</li>\r\n			<li>We will collect and use personal information solely for the purpose of fulfilling the purposes specified by us and for other compatible purposes, unless we obtain the consent of the person in question or as required by law.</li>\r\n			<li>We will only retain personal information as long as necessary for the fulfillment of those purposes.</li>\r\n			<li>We will collect personal information by legal and fair means and, where appropriate, with the knowledge or consent of the person concerned.</li>\r\n			<li>Personal information must be relevant for the purposes for which it will be used and, to the extent necessary for those purposes, must be accurate, complete and up-to-date.</li>\r\n			<li>We will protect personal information through reasonable security measures against loss or theft, as well as unauthorized access, disclosure, copying, use or modification.</li>\r\n		</ol>\r\n		<p>We are committed to conducting our business in accordance with these principles to ensure that the confidentiality of personal information is protected and maintained.</p>\r\n\r\n	<!-------------------Englis pp--------------------->\r\n\r\n</div>', '<div class=\"body\" style=\"padding: 10px 15px;\">\r\n	<h3 class=\"section-heading\" style=\"text-align: center; font-size: 22px;\">TÉRMINOS Y CONDICIONES</h3>\r\n\r\n	<h4>TÉRMINOS DE USO</h4>\r\n	<p>Al descargar, navegar, acceder o utilizar esta aplicación móvil (\"YoPO\"), usted acepta estar sujeto a estos Términos y condiciones de uso. Nos reservamos el derecho de modificar estos términos y condiciones en cualquier momento. Si no está de acuerdo con alguno de estos Términos y condiciones de uso, debe suspender inmediatamente su acceso a la Aplicación móvil y su uso de los servicios ofrecidos en la Aplicación móvil. El uso continuado de la Aplicación móvil constituirá la aceptación de estos Términos y condiciones de uso, que pueden modificarse periódicamente.</p>\r\n\r\n	<h4>Definiciones</h4>\r\n	<p>En estos Términos y condiciones de uso, los siguientes términos en mayúscula tendrán los siguientes significados, excepto cuando el contexto requiera lo contrario: \"Cuenta\" significa una cuenta creada por un Usuario en la Aplicación móvil como parte del Registro. \"Política de privacidad\" significa la política de privacidad establecida en la Cláusula 14 de estos Términos y condiciones de uso. \"Registrarse\" significa crear una cuenta en la aplicación móvil y \"registro\" significa el acto de crear dicha cuenta. \"Servicios\" significa todos los servicios proporcionados por YoPo a través de la Aplicación Móvil a los Usuarios, y \"Servicio\" significa cualquiera de ellos, \"Usuarios\" significa usuarios de la Aplicación Móvil, incluido usted y \"Usuario\" significa cualquiera de ellos</p>\r\n\r\n	<h4>CUESTIONES GENERALES SOBRE LA APLICACIÓN MÓVIL Y LOS SERVICIOS</h4>\r\n	<p>Aplicabilidad de los términos y condiciones: el uso de los Servicios y / o la Aplicación móvil  están sujetos a estos Términos y condiciones de uso.</p>\r\n	<p>Ubicación: la aplicación móvil, los servicios y los canjes están destinados exclusivamente a usuarios que accedan a la aplicación móvil. No garantizamos que los Servicios estén disponibles o sean aptos para su uso , como tampoco nos responsabilizamos por cualquier actividad no deseada como producto del uso de la app, si accede a la Aplicación móvil, utiliza los Servicios  es responsable de las consecuencias y del cumplimiento de todas las leyes aplicables.</p>\r\n	<p>YoPo no es un agencia de empleos por lo cual no somos responsables de salvaguardar a un prestador de servicio mientras realiza un servicio como tampoco asi salvaguardar al usuario que contrata un servicio, es responsabilidad de cada parte involucrada su propia seguridad, solo somos una plataforma de coneccion de servicios.</p>\r\n	<p>Alcance: la aplicación móvil, los servicios son para su uso personal no comercial y no deben utilizarse con fines comerciales. Prevención en el uso: Nos reservamos el derecho de evitar que use la Aplicación móvil y evitar que realice un uso indebido\r\nEquipos y redes: la provisión de los Servicios y la Aplicación móvil requiere el uso de un teléfono móvil necesario para acceder a la Aplicación móvil, necesitará conectividad a Internet y enlaces de telecomunicaciones adecuados. Usted reconoce que los términos del acuerdo con su respectivo proveedor de red móvil continuarán vigentes cuando use la Aplicación Móvil. Como resultado, es posible que el Proveedor de servicios móviles le cobre por el acceso a los servicios de conexión de red durante el tiempo que dure la conexión mientras accede a la Aplicación móvil o a los cargos de terceros que puedan surgir. Usted acepta la responsabilidad de cualquier cargo que surja.\r\nPermiso para usar la aplicación móvil: si no es el pagador de facturas del teléfono móvil o dispositivo portátil que se utiliza para acceder a la aplicación móvil, se supondrá que recibió el permiso del pagador de facturas para usar la aplicación móvil.\r\nLicencia para usar el material: al enviar cualquier texto o imagen (incluidas fotografías) (\"Material\") a través de la Aplicación, usted declara que es el propietario del Material o que tiene la autorización adecuada del propietario del Material para usar, reproducir y distribuirlo Por la presente, nos otorga una licencia mundial, libre de regalías y no exclusiva para usar el Material para promocionar cualquier producto o servicio</p>\r\n\r\n<h4>PAGOS Y COMISIONES</h4>\r\n<p>Tanto el vendor como el user , estan concientes de que deben pagar una comision por servicio de acuerdo al servicio realizado la cual podra variar a traves del tiempo y que sera de conocimiento de ambas partes al momento de contratar un servicio en el lado del user o ofrecer un servicio en el lado del vendor.</p>\r\n<p>adicionalmente en el lado del vendor de su pago despues de descontadas las comisiones de yopo  se le descontara la comision de transferencia de PayPal  que es la plataforma de pagos que utilizara nuestra aplicacion para hacer llegar el dinero al vendor y recaudar el dinero por parte del user. estas comisiones son las listadas por paypal en su plataforma.\r\nque en este momento es 2.9 % del valor de la transferencia mas 0.30  dolar si es nacional 0 4.9 % del valor de la transferencia mas 0.30  dolar si es internacional, estos valores podran variar de acuerdo a las politicas de PayPal sin que esto contraiga responsabilidad para YoPo.</p>\r\n\r\n<h4>ALERTAS Y NOTIFICACIONES DE UBICACIÓN</h4>\r\n<p>Usted acepta recibir notificaciones preprogramadas (\"Alertas de ubicación\") en la Aplicación móvil de los Comerciantes si ha activado los servicios de ubicación en su teléfono móvil u otros dispositivos portátiles (según sea el caso).</p>\r\n\r\n<h4>TUS OBLIGACIONES</h4>\r\n\r\n<ul>\r\n	<li>Términos del usuario: Usted acepta (y deberá cumplir) los términos y condiciones del usuario , que pueden modificarse de vez en cuando.\r\n	Información precisa: Usted garantiza que toda la información proporcionada en el Registro y contenida como parte de su Cuenta es verdadera, completa y precisa y que nos informará de inmediato sobre cualquier cambio en dicha información actualizando la información en su Cuenta.\r\n	Contenido en la Aplicación móvil y el Servicio: es su responsabilidad asegurarse de que cualquier producto o información disponible a través de la Aplicación móvil o los Servicios cumpla con sus requisitos específicos antes de realizar cualquier servicio.\r\n	Prohibiciones en relación con el uso de los Servicios o la Aplicación móvil: sin limitación, usted se compromete a no usar ni permitir que otra persona use los Servicios o la Aplicación móvil</li>\r\n	<li>para enviar o recibir cualquier servicio que no sea civil o de buen gusto\r\n	para enviar o recibir cualquier servicio que sea amenazante, gravemente ofensivo, de carácter indecente, obsceno o amenazante, blasfemo o difamatorio de cualquier persona, en desacato a la corte o en violación de la confianza, derechos de autor, derechos de personalidad, publicidad o privacidad o cualquier otros derechos de terceros;</li>\r\n	<li>para enviar o recibir cualquier servicio que fomente conductas que se considerarían un delito penal, darían lugar a responsabilidad civil, o que serían contrarias a la ley o infringirían los derechos de un tercero en cualquier país del mundo;</li>\r\n	<li>para enviar o recibir cualquier servicio que sea técnicamente dañino o\r\n	para un propósito diferente al que los diseñamos o pretendemos que se usen;</li>\r\n	<li>para cualquier propósito fraudulento;</li>\r\n	<li>aparte de cumplir con las prácticas aceptadas de Internet y las prácticas de cualquier red conectada;\r\n	de cualquier manera que se calcule para incitar al odio contra cualquier minoría étnica, religiosa o de otro tipo, o que se calcule de manera adversa para afectar a cualquier individuo, grupo o entidad; o\r\n	de tal manera que, o cometa cualquier acto que imponga o imponga, una carga irrazonable o desproporcionadamente grande en nuestra infraestructura.</li>\r\n	<li>revender cualquier servicio;</li>\r\n	<li>proporcionar datos falsos, incluidos nombres, direcciones y datos de contacto falsos, y utilizar fraudulentamente números de tarjetas de crédito / débito;</li>\r\n	<li>intentar eludir nuestra seguridad o red, incluido el acceso a datos que no están destinados a usted, iniciar sesión en un servidor o cuenta a la que no está autorizado expresamente para acceder o probar la seguridad de otras redes (como ejecutar un escaneo de puertos);</li>\r\n	<li>ejecutar cualquier forma de monitoreo de red que interceptará datos no destinados a usted;</li>\r\n	<li>entablar interacciones o transacciones fraudulentas con nosotros o con un Comerciante (incluida la interacción o transacción supuestamente en nombre de un tercero en el que no tiene autoridad para vincular a ese tercero o si pretende ser un tercero);</li>\r\n	<li>extraer datos o piratear la aplicación móvil;</li>\r\n	<li>usar los Servicios o la Aplicación móvil en incumplimiento de estos Términos y condiciones de uso;</li>\r\n	<li>participar en cualquier actividad ilegal relacionada con el uso de la Aplicación móvil o los Servicios; o</li>\r\n	<li>participar en cualquier conducta que, en nuestra opinión razonable y exclusiva, restrinja o inhiba a cualquier otro cliente de usar o disfrutar adecuadamente la Aplicación o Servicios Móviles</li>\r\n</ul>\r\n\r\n<h4>NORMAS SOBRE EL USO DEL SERVICIO Y LA APLICACIÓN MÓVIL</h4>\r\n<p>Haremos todos los esfuerzos razonables para corregir cualquier error u omisión tan pronto como sea posible después de ser notificado de ellos. Sin embargo, no garantizamos que los Servicios o la Aplicación móvil estén libres de fallas, y no aceptamos responsabilidad por tales fallas, errores u omisiones. En caso de cualquier error, falla u omisión, debe informarlo comunicándose con nosotros al 18018880954.</p>\r\n<p>No garantizamos que su uso de los Servicios o la Aplicación móvil será ininterrumpido y no garantizamos que cualquier información (o mensaje) transmitida a través de los Servicios o la Aplicación móvil se transmitirá de manera precisa, confiable, oportuna o al ahí. A pesar de que intentaremos permitir el acceso ininterrumpido a los Servicios y la Aplicación móvil, el acceso a los Servicios y la Aplicación móvil puede suspenderse, restringirse o cancelarse en cualquier momento.</p>\r\n<p>No garantizamos que los Servicios y la Aplicación móvil estén libres de virus o cualquier otra cosa que pueda tener un efecto nocivo en cualquier tecnología.</p>\r\n<p>Nos reservamos el derecho de cambiar, modificar, sustituir, suspender o eliminar sin previo aviso cualquier información o Servicios en la Aplicación móvil de vez en cuando. Su acceso a la Aplicación móvil y / o los Servicios también puede restringirse ocasionalmente para permitir reparaciones, mantenimiento o la introducción de nuevas instalaciones o servicios. Intentaremos restablecer dicho acceso tan pronto como sea razonablemente posible. Para evitar dudas, nos reservamos el derecho de retirar cualquier información o Servicios de la Aplicación móvil en cualquier momento.</p>\r\n<p>Nos reservamos el derecho de bloquear el acceso y / o editar o eliminar cualquier material que, en nuestra opinión razonable, pueda dar lugar a una violación de estos Términos y condiciones de uso.</p>\r\n\r\n<h4>SUSPENSIÓN Y TERMINACIÓN</h4>\r\n<p>Si usa (o cualquier otra persona que no sea usted, con su permiso, usa) la Aplicación móvil, cualquier Servicio en contravención de estos Términos y condiciones de uso, podemos suspender su uso de los Servicios y / o la Aplicación móvil.</p>\r\n<p>Si suspendemos los Servicios o la Aplicación móvil, podemos negarnos a restaurar los Servicios o la Aplicación móvil para su uso hasta que recibamos una garantía de su parte, en una forma que consideremos aceptable, de que no habrá más incumplimiento de las disposiciones de estos Términos y condiciones de uso.</p>\r\n<p>Sin limitación a cualquier otra cosa en esta Cláusula 8, tendremos derecho inmediatamente o en cualquier momento (en su totalidad o en parte) a: (a) suspender los Servicios y / o la Aplicación móvil; (b) suspender su uso de los Servicios y / o la Aplicación móvil; y / o (c) suspender el uso de los Servicios y / o la Aplicación móvil para personas que creemos que están conectadas (de cualquier manera) a usted, si:</p>\r\n<p>usted comete un incumplimiento de estos Términos y condiciones de uso;</p>\r\n<p>sospechamos, por razones razonables, que usted ha cometido o podría cometer una violación de estos Términos y condiciones de uso; o</p>\r\n<p>Sospechamos, por motivos razonables, que puede haber cometido o cometer un fraude contra nosotros o cualquier persona.</p>\r\n<p>Nuestros derechos bajo esta Cláusula 8 no prejuzgarán ningún otro derecho o recurso que podamos tener con respecto a cualquier incumplimiento o cualquier derecho, obligación o responsabilidad acumulados antes de la terminación.</p>\r\n\r\n<h4>RENUNCIA Y EXCLUSIÓN DE RESPONSABILIDAD</h4>\r\n<p>La Aplicación móvil, los Servicios, la información sobre la Aplicación móvil y el uso de todas las instalaciones relacionadas se proporcionan \"tal cual, según estén disponibles\" sin ninguna garantía, ya sea expresa o implícita.</p>\r\n<p>En la mayor medida permitida por la ley aplicable, renunciamos a todas las representaciones y garantías relacionadas con la Aplicación móvil y su contenido, incluso en relación con cualquier inexactitud u omisión en la Aplicación móvil, garantías de comerciabilidad, calidad, idoneidad para un propósito particular, precisión , disponibilidad, no infracción o garantías implícitas del curso de negociación o uso del comercio.</p>\r\n<p>No garantizamos que la aplicación móvil siempre sea accesible, ininterrumpida, oportuna, segura, libre de errores o libre de virus informáticos u otro código invasivo o dañino o que la aplicación móvil no se verá afectada por ningún acto de Dios u otra fuerza mayor eventos, incluida la incapacidad para obtener o la escasez de materiales necesarios, instalaciones de equipos, energía o telecomunicaciones, falta de equipos o instalaciones de telecomunicaciones y falla de la tecnología de la información o equipos o instalaciones de telecomunicaciones.</p>\r\n<p>Si bien podemos hacer esfuerzos razonables para incluir información precisa y actualizada en la Aplicación móvil, no garantizamos ni representamos su exactitud, oportunidad o integridad.</p>\r\n<p>No seremos confiables por ningún acto u omisión de terceros, independientemente de su causa, ni por daños directos, indirectos, incidentales, especiales, consecuentes o punitivos, independientemente de su causa, como resultado de la aplicación móvil y los servicios ofrecidos en la aplicación móvil, su acceso, uso o incapacidad para usar la aplicación móvil o los servicios ofrecidos en la aplicación móvil, la dependencia o la descarga de la aplicación móvil y / o servicios, o cualquier demora, inexactitud en la información o en su transmisión, incluidos, entre otros, daños por pérdida de negocios o ganancias, uso, datos u otros intangibles, incluso si se nos ha informado de la posibilidad de dichos daños.</p>\r\n<p>No seremos confiables en el contrato, agravio (incluyendo negligencia o incumplimiento del deber legal) o de cualquier otra forma y cualquiera que sea la causa del mismo, por cualquier pérdida o daño indirecto, consecuente, colateral, especial o incidental sufrido o incurrido por usted en relación con el Aplicación móvil y estos Términos y condiciones de uso. A los fines de estos Términos y condiciones de uso, la pérdida o daño indirecto o consecuente incluye, sin limitación, pérdida de ingresos, ganancias, ahorros anticipados o negocios, pérdida de datos o buena voluntad, pérdida de uso o valor de cualquier equipo, incluido el software, reclamos de terceros, y todos los costos y gastos asociados e incidentales.</p>\r\n\r\n<h4>INDEMNIDAD</h4>\r\n<p>Usted acepta indemnizar y mantenernos indemnizados contra cualquier reclamo, acción, demanda o procedimiento presentado o amenazado con ser presentado contra nosotros que sea causado por (a) su uso de los Servicios, (b) el uso de cualquier otra parte de los Servicios que utilizan su ID de usuario, PIN de verificación y / o cualquier número de identificación asignado por YoPo , y / o (c) su incumplimiento de cualquiera de estos Términos y Condiciones de Uso, y para pagarnos daños, costos e intereses en conexión con dicho reclamo, acción, demanda o procedimiento.</p>\r\n\r\n<h4>DERECHOS DE PROPIEDAD INTELECTUAL</h4>\r\n<p>Todo el contenido editorial, información, fotografías, ilustraciones, material gráfico y otros materiales gráficos, y nombres, logotipos y marcas registradas en la Aplicación móvil están protegidos por leyes de derechos de autor y / u otras leyes y / o tratados internacionales, y nos pertenecen y / o nuestros proveedores, según sea el caso. Estas obras, logotipos, gráficos, sonidos o imágenes no pueden copiarse, reproducirse, retransmitirse, distribuirse, difundirse, venderse, publicarse, transmitirse o distribuirse, ya sea total o parcialmente, a menos que lo autoricemos expresamente nosotros y / o nuestros proveedores, como caso puede ser.</p>\r\n<p>Nada de lo contenido en la Aplicación móvil debe interpretarse como una concesión, por implicación, impedimento o de otra manera, de cualquier licencia o derecho de uso de cualquier marca comercial que se muestre en la Aplicación móvil sin nuestro permiso por escrito. Se prohíbe el uso indebido de cualquier marca comercial o cualquier otro contenido que se muestre en la Aplicación móvil.</p>\r\n<p>No dudaremos en emprender acciones legales contra el uso no autorizado de nuestras marcas comerciales, nombres o símbolos para preservar y proteger sus derechos en el asunto. Todos los derechos no otorgados expresamente en este documento están reservados. Otros nombres de productos y compañías mencionados aquí también pueden ser marcas comerciales de sus respectivos propietarios.</p>\r\n<p>Las exclusiones y limitaciones anteriores se aplican solo en la medida permitida por la ley. Ninguno de sus derechos legales como consumidor que no puede ser excluido o limitado se ve afectado.</p>\r\n<p>A pesar de nuestros esfuerzos para garantizar que nuestro sistema sea seguro, usted reconoce que todas las transferencias electrónicas de datos son potencialmente susceptibles de ser interceptadas por otros. No podemos y no garantizamos que las transferencias de datos de conformidad con la Aplicación móvil o el correo electrónico transmitido hacia y desde nosotros no sean monitoreados o leídos por otros.</p>\r\n\r\n<h4>ENMIENDAS</h4>\r\n<p>Periódicamente podemos realizar cambios en el contenido de la Aplicación móvil, incluidas las descripciones y precios de los bienes y servicios anunciados, en cualquier momento y sin previo aviso. No asumimos ninguna responsabilidad por los errores u omisiones en el contenido de la Aplicación móvil.</p>\r\n<p>Nos reservamos el derecho de modificar estos Términos y condiciones de uso de vez en cuando sin previo aviso. Los Términos y condiciones de uso revisados ??se publicarán en la Aplicación móvil y entrarán en vigencia a partir de la fecha de dicha publicación. Se recomienda revisar estos términos y condiciones periódicamente, ya que son vinculantes para usted.</p>\r\n\r\n<h4>LEY APLICABLE Y JURISDICCIÓN</h4>\r\n<p>Se puede acceder a la aplicación móvil desde todos los países del mundo donde la tecnología local lo permita. Como cada uno de estos lugares tiene leyes diferentes, al acceder a la Aplicación móvil, tanto usted como nosotros aceptamos que las leyes de EEUU, sin tener en cuenta los conflictos de principios legales, se aplicarán a todos los asuntos relacionados con el uso de la Aplicación móvil.</p>\r\n<p>Usted acepta y acepta que tanto usted como nosotros nos someteremos a la jurisdicción exclusiva de los tribunales de EEUU con respecto a cualquier disputa que surja o esté relacionada con estos Términos y condiciones de uso.</p>\r\n\r\n<h4>Política de privacidad</h4>\r\n<p>El acceso a la Aplicación móvil y el uso de los Servicios ofrecidos en la Aplicación móvil por YoPo  está sujeto a esta Política de privacidad. Al acceder a la Aplicación móvil y al continuar utilizando los Servicios ofrecidos, se considera que ha aceptado esta Política de privacidad y, en particular, se ha dado su consentimiento para que usemos y divulguemos su información personal de la manera prescrita en esta Privacidad Política y para los fines establecidos en las Cláusulas 3.7. y / o 4.1. Nos reservamos el derecho de modificar esta Política de privacidad de vez en cuando. Si no está de acuerdo con alguna parte de esta Política de privacidad, debe interrumpir de inmediato su acceso a la Aplicación móvil y su uso de los Servicios.</p>\r\n\r\n<p>Como parte del funcionamiento normal de nuestros Servicios, recopilamos, usamos y, en algunos casos, divulgamos su información a terceros. En consecuencia, hemos desarrollado esta Política de privacidad para que pueda comprender cómo recopilamos, usamos, comunicamos, divulgamos y hacemos uso de su información personal cuando utiliza los Servicios en la Aplicación móvil:</p>\r\n\r\n<ol>\r\n	<li>Antes o en el momento de recopilar información personal, identificaremos los fines para los que se recopila la información.</li>\r\n	<li>Recopilaremos y utilizaremos información personal únicamente con el objetivo de cumplir con los fines especificados por nosotros y para otros fines compatibles, a menos que obtengamos el consentimiento de la persona en cuestión o según lo exija la ley.</li>\r\n	<li>Solo retendremos información personal el tiempo que sea necesario para el cumplimiento de esos propósitos.</li>\r\n	<li>Recopilaremos información personal por medios legales y justos y, cuando corresponda, con el conocimiento o consentimiento de la persona interesada.</li>\r\n	<li>La información personal debe ser relevante para los fines para los que se va a utilizar y, en la medida necesaria para esos fines, debe ser precisa, completa y actualizada.</li>\r\n	<li>Protegeremos la información personal mediante medidas de seguridad razonables contra pérdida o robo, así como acceso, divulgación, copia, uso o modificación no autorizados.</li>\r\n</ol>\r\n<p>Estamos comprometidos a llevar a cabo nuestro negocio de acuerdo con estos principios para asegurar que la confidencialidad de la información personal esté protegida y mantenida.</p>\r\n</div>');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_image` varchar(255) NOT NULL,
  `price` float(10,2) NOT NULL,
  `offer` varchar(255) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `product_name`, `product_image`, `price`, `offer`, `user_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Adf', '1GvucnwbR2_1604661397.jpg', 0.00, '134', 20, '2020-11-06 06:16:37', '2020-11-06 06:16:37', NULL),
(2, 'Product 1', 'xpM1sHSddT_1604737881.jpg', 123.00, '345', 20, '2020-11-07 03:31:21', '2020-11-07 03:31:21', NULL),
(3, 'Product 34', '99Zt0TmXOa_1605866548.jpg', 123.00, '110', 20, '2020-11-20 05:02:28', '2020-11-20 05:02:28', NULL),
(4, 'SEPHORA COLLECTION Into the Stars Palette - A 130-piece palette', '6CB8r5IooL_1606171293.jpg', 550.00, '450', 31, '2020-11-23 17:41:33', '2020-11-23 17:41:33', NULL),
(5, 'Abcde', '8POrsCpU1p_1606305696.jpg', 500.00, 'Free', 20, '2020-11-25 07:01:36', '2020-11-25 07:01:36', NULL),
(6, 'Gzjanana', 'Moo8NsXL7h_1606309644.jpg', 0.00, 'Gahaba', 20, '2020-11-25 08:07:24', '2020-11-25 08:07:24', NULL),
(7, 'Cutting trail', 'zxjqqVroe3_1606393881.jpg', 200.00, 'Free trail', 34, '2020-11-26 07:31:21', '2020-11-26 07:31:21', NULL),
(8, 'Hair color', 'Jp6hvJbbZe_1606394836.jpg', 550.00, '10% discount', 34, '2020-11-26 07:47:16', '2020-11-26 07:47:16', NULL),
(9, 'Makeup', 'h4klieisp5_1608044717.jpg', 120.00, '90', 41, '2020-12-15 10:05:17', '2020-12-15 10:05:17', NULL),
(10, 'Makeup', 'h9PhTxoVu6_1608044748.jpg', 77.00, '50', 41, '2020-12-15 10:05:48', '2020-12-15 10:05:48', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `supports`
--

CREATE TABLE `supports` (
  `support_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `support_type` enum('1','2') NOT NULL,
  `user_name` varchar(225) DEFAULT NULL,
  `phone_number` varchar(225) DEFAULT NULL,
  `email` varchar(225) DEFAULT NULL,
  `message` text,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supports`
--

INSERT INTO `supports` (`support_id`, `user_id`, `support_type`, `user_name`, `phone_number`, `email`, `message`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 7, '2', 'Ganesh Vendor', '1234567890', 'ganesh_vendor@mailinator.com', 'This', '2020-10-09 03:33:32', '2020-10-09 03:33:32', NULL),
(2, 7, '1', 'Ganesh Vendor', '1234567890', 'ganesh_vendor@mailinator.com', 'This', '2020-10-09 03:33:53', '2020-10-09 03:33:53', NULL),
(3, 32, '2', 'ajay', '9630156770', 'ajay2@gmail.com', 'zxcsdwrewrwe', '2020-11-21 04:52:38', '2020-11-21 04:52:38', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `transaction_history`
--

CREATE TABLE `transaction_history` (
  `transaction_history_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `appointment_id` int(10) DEFAULT NULL,
  `paypal_charge` decimal(10,2) DEFAULT NULL,
  `payment_method` enum('1','2','3','4') NOT NULL COMMENT '1=card,2=paypal',
  `transaction_id` varchar(225) DEFAULT NULL,
  `currency` varchar(20) DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `transaction_status` enum('1','2') DEFAULT NULL COMMENT '1=success,2=failed',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `transaction_history`
--

INSERT INTO `transaction_history` (`transaction_history_id`, `user_id`, `appointment_id`, `paypal_charge`, `payment_method`, `transaction_id`, `currency`, `amount`, `transaction_status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 6, 1, 50.00, '2', '31S88999489309629', 'USD', 10.00, '1', '2020-09-21 04:24:52', '2020-09-21 04:24:52', NULL),
(2, 6, 2, 50.00, '1', '6f3ngrcw', 'SAR', 10.00, '1', '2020-09-21 04:25:45', '2020-09-21 04:25:45', NULL),
(3, 6, 4, 50.00, '1', 'ey758k74', 'SAR', 10.00, '1', '2020-09-21 04:27:35', '2020-09-21 04:27:35', NULL),
(4, 6, 1, 50.00, '1', 'm2z63wsv', 'SAR', 10.00, '1', '2020-09-26 01:48:20', '2020-09-26 01:48:20', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role` enum('1','2','3') COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '1=admin,2=users,3=vendor',
  `vendor_type` enum('1','2') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1' COMMENT '1=artist,2=Salon Owner',
  `specialist` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `social_type` enum('1','2') COLLATE utf8mb4_unicode_ci NOT NULL,
  `social_id` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verify_status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `phone_number` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` enum('1','2') COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '1=male,2=female',
  `profile_image` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lat` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lng` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `otp` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `otp_verify_status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '0=otp_not_verified,1=otp_verified',
  `active_status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1' COMMENT '0=deactivate,1 activate',
  `approve_status` enum('0','1','2') COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '0=pending,1=approved,2=rejected',
  `api_status` enum('0','1','2') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '0=step_1,1=step_2,2=step_3',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notification_status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1' COMMENT '0=off,1=on',
  `device_type` enum('android','ios','web','other') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `device_token` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `language` enum('en','es') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'en',
  `description` text COLLATE utf8mb4_unicode_ci,
  `registration_card` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `document_1` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `document_2` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `document_3` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `availability_status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `service_status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role`, `vendor_type`, `specialist`, `social_type`, `social_id`, `user_name`, `first_name`, `last_name`, `email`, `email_verify_status`, `phone_number`, `gender`, `profile_image`, `address`, `lat`, `lng`, `otp`, `otp_verify_status`, `active_status`, `approve_status`, `api_status`, `password`, `remember_token`, `notification_status`, `device_type`, `device_token`, `language`, `description`, `registration_card`, `document_1`, `document_2`, `document_3`, `availability_status`, `service_status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '1', '1', NULL, '', NULL, 'Admin', 'Admin', 'Admin', 'admin@mailinator.com', '1', '95751645491', NULL, 'Image_1', NULL, '22.719568', '75.857727', '0491', '0', '1', '1', '0', '$2y$10$ExamjU1FsSKJ.gJBmj.7Pen0jRyg7Y4RrNxQTkkMifAeDY3emahTm', NULL, '1', 'web', 'mamama', 'en', NULL, 'ZRpRKMmO0E_1600506355.jpg', 'GrwPqBKocj_1600506415.jpg', 'jXS58zIDpw_1600506422.jpg', '4iSCS9wB9P_1600506436.jpg', '0', '0', '2020-02-09 18:30:00', '2020-11-10 04:57:31', NULL),
(20, '3', '2', '1', '1', NULL, 'raju  singh', NULL, NULL, 'raju@gmail.com', '1', '9852147965', NULL, 'hKsflQo2d2_1606305638.jpg', '\"Pukhraj Corporation, 715, Navlakha Rd, Navlakha, Indore, Madhya Pradesh 452001, India\"', '22.7003992', '75.87599589999999', '2279', '0', '1', '0', '0', '$2y$10$ExamjU1FsSKJ.gJBmj.7Pen0jRyg7Y4RrNxQTkkMifAeDY3emahTm', NULL, '1', 'ios', 'null', 'en', 'xcxvdfgfd fgfd fgfdg fgfdg', NULL, NULL, NULL, NULL, '1', '1', '2020-11-04 03:02:05', '2020-12-18 01:05:21', NULL),
(28, '2', '1', NULL, '1', NULL, 'ramlal', NULL, NULL, 'ramlal@gmail.com', '1', '8349877407', NULL, 'NU6yCkkrUR_1606309943.jpg', '\"Navlakha Rd, Indore, Madhya Pradesh, India\"', '22.7139', NULL, '3184', '0', '1', '0', '0', '$2y$10$ExamjU1FsSKJ.gJBmj.7Pen0jRyg7Y4RrNxQTkkMifAeDY3emahTm', NULL, '1', 'ios', 'test', 'en', 'asa sdaksd', NULL, NULL, NULL, NULL, '0', '0', '2020-11-10 07:24:35', '2020-12-18 01:04:38', NULL),
(31, '3', '2', '2', '1', NULL, 'Beauty Center', NULL, NULL, 'null', '1', '0533755044', NULL, 'vD6UZjUAM5_1606320594.jpg', 'Saudi Arabia, Riyadh', '23.3423', '34.2323', '8705', '0', '1', '0', '0', '$2y$10$ExamjU1FsSKJ.gJBmj.7Pen0jRyg7Y4RrNxQTkkMifAeDY3emahTm', NULL, '1', 'android', 'null', 'en', 'مركز الجمال لخدمات التجميل والعناية بالبشرة والشعر', 'ULxsEjkkio_1605169092.jpg', '2DZjOO12CQ_1605169160.jpg', 'oKHoL9Fayv_1605169146.jpg', 'y4ERuizOMW_1605169151.jpg', '1', '1', '2020-11-12 03:17:11', '2020-11-25 11:09:54', NULL),
(32, '2', '1', NULL, '1', NULL, 'ajay', NULL, NULL, 'ajay2@gmail.com', '1', '9630156770', '1', NULL, '\"Opp. Jalgaon Jewellers,Sarafa Bazar,Sunka Chowk, Narsinghpur, Madhya Pradesh 487001, India\"', '22.9455', NULL, '7753', '0', '1', '0', '0', '$2y$10$ExamjU1FsSKJ.gJBmj.7Pen0jRyg7Y4RrNxQTkkMifAeDY3emahTm', NULL, '1', 'android', 'null', 'en', 'test in ambuj', NULL, NULL, NULL, NULL, '0', '0', '2020-11-21 01:06:54', '2020-11-21 01:50:23', NULL),
(33, '3', '1', '2', '1', NULL, 'Asim', NULL, NULL, NULL, '1', '056 118 0282', NULL, NULL, '109 Angus Rd, Monroe, LA 71202, USA', '32.3794408', NULL, '5150', '0', '1', '0', '0', '$2y$10$ExamjU1FsSKJ.gJBmj.7Pen0jRyg7Y4RrNxQTkkMifAeDY3emahTm', NULL, '1', 'android', 'null', 'en', NULL, 'RZYcPa4qJX_1606315043.jpg', 'r7IP6RrTnR_1606315052.jpg', '49BfbrVyRE_1606315058.jpg', 'MjpIdckxRg_1606315064.jpg', '1', '1', '2020-11-25 09:37:05', '2020-11-25 09:38:56', NULL),
(34, '3', '1', NULL, '1', NULL, 'Buety 123', NULL, NULL, NULL, '1', '9584741316', NULL, 'X1bapdnckW_1606394741.jpg', '109 Angus Rd, Monroe, LA 71202, USA', '32.3794408', NULL, '8797', '0', '1', '0', '0', '$2y$10$ExamjU1FsSKJ.gJBmj.7Pen0jRyg7Y4RrNxQTkkMifAeDY3emahTm', NULL, '1', 'android', 'null', 'en', NULL, NULL, NULL, NULL, NULL, '1', '1', '2020-11-26 00:37:51', '2020-11-26 07:45:41', NULL),
(39, '3', '1', '1', '1', NULL, 'Codemeg Soft Solution', NULL, NULL, NULL, '1', '8959070299', NULL, NULL, '\"Pukhraj Corporation, 715, Navlakha Rd, Navlakha, Indore, Madhya Pradesh 452001, India\"', '22.7003992', NULL, '2589', '1', '1', '1', '2', '$2y$10$oA8wDgQ5k71pU/cD.DxDAOvYonl7osTeNwSvleDkYNL65lZ8E0n8W', NULL, '1', 'ios', '953df29c757facafdb154e5e5ea37e7df2eb7282c8827fccc80bd01fe5c582cd', 'en', NULL, 'ULjqsPAnsk_1607945504.jpg', 'sp3192h24i_1607945557.jpg', 'pUPYQWdTtM_1607945589.jpg', 'rZGIv23nbk_1607945608.jpg', '1', '1', '2020-12-14 06:30:27', '2020-12-14 06:50:38', NULL),
(40, '2', '1', NULL, '1', NULL, 'Inspiring', NULL, NULL, '7974682508', '1', '7974682508', '1', NULL, '301, Navlakha Rd, Pukhraj Corporation, Navlakha, Indore, Madhya Pradesh 452001, India', '22.700325956704443', NULL, '2646', '0', '1', '0', '0', '$2y$10$hFGjqjC1Hb08.ZDqvx.XtOUJzAkSrl1z.ak4enOttB2psi48P.xRe', NULL, '1', 'ios', '1a675b6629ea8ad768cf11299568f6967b18362ef968955ce4f727dc5c013169', 'en', NULL, NULL, NULL, NULL, NULL, '0', '0', '2020-12-14 06:55:55', '2020-12-14 06:55:55', NULL),
(41, '3', '1', NULL, '1', NULL, 'Jamali', NULL, NULL, NULL, '1', '0533355555', NULL, 'uHfVzPFseh_1608044908.jpg', '4302 Ath Thumamah Road, Al Munsiyah, Riyadh 13421 6955, Saudi Arabia', '24.84595486336168', NULL, '3574', '0', '1', '0', '0', '$2y$10$cV4KQ.lHT5Kt57uE/.dYbuY3ZNhhvIVFRo.yggN43QEv5Y4JdK0uq', NULL, '1', 'ios', '32482c43f93e11f337e1030a9b5b766faf12f2ad5b81f2b6828d87fecf215228', 'en', NULL, 'AKfLfPIn3J_1608044317.jpg', 'bAloCWrEyP_1608044342.jpg', 'b1QpuP0z5Y_1608044354.jpg', 'g85i5caJR4_1608044385.jpg', '1', '1', '2020-12-15 09:58:12', '2020-12-15 10:08:28', NULL),
(42, '2', '1', NULL, '1', NULL, 'Jhgfdsa', NULL, NULL, '0533335555', '1', '0533335555', '2', NULL, '3884, Al Munsiyah, Riyadh 13249 7259, Saudi Arabia', '24.83551749669017', NULL, '4774', '0', '1', '0', '0', '$2y$10$JcVYSEMaF9Xpq908LyDAK.O5OWXRbVIPU6Uai8LagPwQie0Za8vMW', NULL, '1', 'ios', '869380c823456fc7889b626243a91630d7558356ada7651902e5fdebc7b7c957', 'en', 'Ghfffdv', NULL, NULL, NULL, NULL, '0', '0', '2020-12-15 14:24:26', '2020-12-15 14:50:32', NULL),
(43, '3', '1', '1', '1', NULL, 'navlakh', NULL, NULL, NULL, '1', '9993469469', NULL, NULL, '132, Nemi Nagar Extension, Lokmanya Nagar, Indore, Madhya Pradesh 452009, India', '22.691652899999998', NULL, '7362', '0', '1', '0', '0', '$2y$10$5OoVCTbk9wREo2wzODpguOPKAABcOANKpZbOkJaI3tR0BiBr6EAc.', NULL, '1', 'android', 'null', 'en', NULL, NULL, NULL, NULL, NULL, '0', '0', '2020-12-18 02:06:17', '2020-12-18 02:06:17', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vendor_days`
--

CREATE TABLE `vendor_days` (
  `vendor_day_id` int(10) UNSIGNED NOT NULL,
  `vendor_id` int(10) UNSIGNED NOT NULL,
  `day_id` int(10) DEFAULT NULL,
  `day_name` varchar(225) DEFAULT NULL,
  `day_status` enum('0','1') DEFAULT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vendor_days`
--

INSERT INTO `vendor_days` (`vendor_day_id`, `vendor_id`, `day_id`, `day_name`, `day_status`, `start_time`, `end_time`, `created_at`, `updated_at`, `deleted_at`) VALUES
(36, 7, 1, 'Sunday', '1', '09:45:00', '06:45:00', '2020-10-26 12:33:50', '2020-10-26 12:33:50', NULL),
(37, 7, 2, 'Monday', '0', '09:45:00', '06:45:00', '2020-10-26 12:33:50', '2020-10-26 12:33:50', NULL),
(38, 7, 3, 'Tuesday', '1', '09:45:00', '06:45:00', '2020-10-26 12:33:50', '2020-10-26 12:33:50', NULL),
(39, 7, 4, 'Wednesday', '1', '09:45:00', '06:45:00', '2020-10-26 12:33:50', '2020-10-26 12:33:50', NULL),
(40, 7, 5, 'Thursday', '0', '09:45:00', '06:45:00', '2020-10-26 12:33:50', '2020-10-26 12:33:50', NULL),
(41, 7, 6, 'Friday', '1', '09:45:00', '06:45:00', '2020-10-26 12:33:50', '2020-10-26 12:33:50', NULL),
(42, 7, 7, 'Saturday', '1', '09:45:00', '06:45:00', '2020-10-26 12:33:50', '2020-10-26 12:33:50', NULL),
(43, 17, 1, 'Sunday', '1', '09:45:00', '06:45:00', '2020-10-31 10:36:19', '2020-10-31 10:36:19', NULL),
(44, 17, 2, 'Monday', '1', '09:45:00', '06:45:00', '2020-10-31 10:36:19', '2020-10-31 10:36:19', NULL),
(45, 17, 3, 'Tuesday', '1', '09:45:00', '06:45:00', '2020-10-31 10:36:19', '2020-10-31 10:36:19', NULL),
(46, 17, 4, 'Wednesday', '1', '09:45:00', '06:45:00', '2020-10-31 10:36:19', '2020-10-31 10:36:19', NULL),
(47, 17, 5, 'Thursday', '1', '09:45:00', '06:45:00', '2020-10-31 10:36:19', '2020-10-31 10:36:19', NULL),
(48, 17, 6, 'Friday', '1', '09:45:00', '06:45:00', '2020-10-31 10:36:19', '2020-10-31 10:36:19', NULL),
(49, 17, 7, 'Saturday', '1', '09:45:00', '06:45:00', '2020-10-31 10:36:19', '2020-10-31 10:36:19', NULL),
(50, 18, 1, 'Sunday', '1', '09:45:00', '06:45:00', '2020-10-31 13:10:56', '2020-10-31 13:10:56', NULL),
(51, 18, 2, 'Monday', '1', '09:45:00', '06:45:00', '2020-10-31 13:10:56', '2020-10-31 13:10:56', NULL),
(52, 18, 3, 'Tuesday', '1', '09:45:00', '06:45:00', '2020-10-31 13:10:56', '2020-10-31 13:10:56', NULL),
(53, 18, 4, 'Wednesday', '1', '09:45:00', '06:45:00', '2020-10-31 13:10:56', '2020-10-31 13:10:56', NULL),
(54, 18, 5, 'Thursday', '1', '09:45:00', '06:45:00', '2020-10-31 13:10:56', '2020-10-31 13:10:56', NULL),
(55, 18, 6, 'Friday', '1', '09:45:00', '06:45:00', '2020-10-31 13:10:56', '2020-10-31 13:10:56', NULL),
(56, 18, 7, 'Saturday', '1', '09:45:00', '06:45:00', '2020-10-31 13:10:56', '2020-10-31 13:10:56', NULL),
(71, 22, 1, 'Sunday', '1', '09:45:00', '06:45:00', '2020-11-04 16:05:56', '2020-11-04 16:05:56', NULL),
(72, 22, 2, 'Monday', '0', '09:45:00', '06:45:00', '2020-11-04 16:05:56', '2020-11-04 16:05:56', NULL),
(73, 22, 3, 'Tuesday', '0', '09:45:00', '06:45:00', '2020-11-04 16:05:56', '2020-11-04 16:05:56', NULL),
(74, 22, 4, 'Wednesday', '0', '09:45:00', '06:45:00', '2020-11-04 16:05:56', '2020-11-04 16:05:56', NULL),
(75, 22, 5, 'Thursday', '0', '09:45:00', '06:45:00', '2020-11-04 16:05:56', '2020-11-04 16:05:56', NULL),
(76, 22, 6, 'Friday', '0', '09:45:00', '06:45:00', '2020-11-04 16:05:56', '2020-11-04 16:05:56', NULL),
(77, 22, 7, 'Saturday', '0', '09:45:00', '06:45:00', '2020-11-04 16:05:56', '2020-11-04 16:05:56', NULL),
(99, 33, 1, 'Sunday', '0', '09:45:00', '06:45:00', '2020-11-25 15:38:25', '2020-11-25 15:38:25', NULL),
(100, 33, 2, 'Monday', '0', '09:45:00', '06:45:00', '2020-11-25 15:38:25', '2020-11-25 15:38:25', NULL),
(101, 33, 3, 'Tuesday', '0', '09:45:00', '06:45:00', '2020-11-25 15:38:25', '2020-11-25 15:38:25', NULL),
(102, 33, 4, 'Wednesday', '0', '09:45:00', '06:45:00', '2020-11-25 15:38:25', '2020-11-25 15:38:25', NULL),
(103, 33, 5, 'Thursday', '0', '09:45:00', '06:45:00', '2020-11-25 15:38:25', '2020-11-25 15:38:25', NULL),
(104, 33, 6, 'Friday', '0', '09:45:00', '06:45:00', '2020-11-25 15:38:25', '2020-11-25 15:38:25', NULL),
(105, 33, 7, 'Saturday', '0', '09:45:00', '06:45:00', '2020-11-25 15:38:25', '2020-11-25 15:38:25', NULL),
(106, 31, 1, 'Sunday', '1', '17:00:00', '23:59:00', '2020-11-26 02:46:51', '2020-11-26 02:46:51', NULL),
(107, 31, 2, 'Monday', '1', '09:45:00', '06:45:00', '2020-11-26 02:46:51', '2020-11-26 02:46:51', NULL),
(108, 31, 3, 'Tuesday', '1', '09:45:00', '18:45:00', '2020-11-26 02:46:51', '2020-11-26 02:46:51', NULL),
(109, 31, 4, 'Wednesday', '1', '09:45:00', '06:45:00', '2020-11-26 02:46:51', '2020-11-26 02:46:51', NULL),
(110, 31, 5, 'Thursday', '1', '09:45:00', '06:45:00', '2020-11-26 02:46:51', '2020-11-26 02:46:51', NULL),
(111, 31, 6, 'Friday', '1', '09:45:00', '06:45:00', '2020-11-26 02:46:51', '2020-11-26 02:46:51', NULL),
(112, 31, 7, 'Saturday', '1', '11:24:00', '06:45:00', '2020-11-26 02:46:51', '2020-11-26 02:46:51', NULL),
(134, 34, 1, 'Sunday', '1', '07:15:00', '12:00:00', '2020-11-26 13:45:01', '2020-11-26 13:45:01', NULL),
(135, 34, 2, 'Monday', '1', '09:45:00', '06:45:00', '2020-11-26 13:45:01', '2020-11-26 13:45:01', NULL),
(136, 34, 3, 'Tuesday', '0', '09:45:00', '06:45:00', '2020-11-26 13:45:01', '2020-11-26 13:45:01', NULL),
(137, 34, 4, 'Wednesday', '0', '09:45:00', '06:45:00', '2020-11-26 13:45:01', '2020-11-26 13:45:01', NULL),
(138, 34, 5, 'Thursday', '0', '09:45:00', '06:45:00', '2020-11-26 13:45:01', '2020-11-26 13:45:01', NULL),
(139, 34, 6, 'Friday', '1', '09:45:00', '06:45:00', '2020-11-26 13:45:01', '2020-11-26 13:45:01', NULL),
(140, 34, 7, 'Saturday', '0', '09:45:00', '06:45:00', '2020-11-26 13:45:01', '2020-11-26 13:45:01', NULL),
(169, 39, 1, 'Sunday', '1', '09:45:00', '17:37:00', '2020-12-14 13:06:09', '2020-12-14 13:06:09', NULL),
(170, 39, 2, 'Monday', '1', '09:45:00', '20:00:00', '2020-12-14 13:06:09', '2020-12-14 13:06:09', NULL),
(171, 39, 3, 'Tuesday', '1', '09:45:00', '06:45:00', '2020-12-14 13:06:09', '2020-12-14 13:06:09', NULL),
(172, 39, 4, 'Wednesday', '1', '09:45:00', '06:45:00', '2020-12-14 13:06:09', '2020-12-14 13:06:09', NULL),
(173, 39, 5, 'Thursday', '1', '09:45:00', '06:45:00', '2020-12-14 13:06:09', '2020-12-14 13:06:09', NULL),
(174, 39, 6, 'Friday', '1', '09:45:00', '06:45:00', '2020-12-14 13:06:09', '2020-12-14 13:06:09', NULL),
(175, 39, 7, 'Saturday', '1', '09:45:00', '06:45:00', '2020-12-14 13:06:09', '2020-12-14 13:06:09', NULL),
(176, 20, 1, 'Sunday', '1', '09:45:00', '18:45:00', '2020-12-15 12:18:42', '2020-12-15 12:18:42', NULL),
(177, 20, 2, 'Monday', '1', '09:45:00', '18:45:00', '2020-12-15 12:18:42', '2020-12-15 12:18:42', NULL),
(178, 20, 3, 'Tuesday', '1', '09:45:00', '18:45:00', '2020-12-15 12:18:42', '2020-12-15 12:18:42', NULL),
(179, 20, 4, 'Wednesday', '1', '09:45:00', '23:40:00', '2020-12-15 12:18:42', '2020-12-15 12:18:42', NULL),
(180, 20, 5, 'Thursday', '1', '09:45:00', '18:45:00', '2020-12-15 12:18:42', '2020-12-15 12:18:42', NULL),
(181, 20, 6, 'Friday', '1', '09:45:00', '18:45:00', '2020-12-15 12:18:42', '2020-12-15 12:18:42', NULL),
(182, 20, 7, 'Saturday', '1', '09:45:00', '18:45:00', '2020-12-15 12:18:42', '2020-12-15 12:18:42', NULL),
(190, 41, 1, 'Sunday', '0', '15:00:00', '23:59:00', '2020-12-15 20:22:10', '2020-12-15 20:22:10', NULL),
(191, 41, 2, 'Monday', '0', '15:00:00', '17:55:00', '2020-12-15 20:22:10', '2020-12-15 20:22:10', NULL),
(192, 41, 3, 'Tuesday', '0', '15:00:00', '06:45:00', '2020-12-15 20:22:10', '2020-12-15 20:22:10', NULL),
(193, 41, 4, 'Wednesday', '0', '15:00:00', '18:08:00', '2020-12-15 20:22:10', '2020-12-15 20:22:10', NULL),
(194, 41, 5, 'Thursday', '0', '15:00:00', '06:45:00', '2020-12-15 20:22:10', '2020-12-15 20:22:10', NULL),
(195, 41, 6, 'Friday', '1', '12:00:00', '06:45:00', '2020-12-15 20:22:10', '2020-12-15 20:22:10', NULL),
(196, 41, 7, 'Saturday', '1', '09:45:00', '06:45:00', '2020-12-15 20:22:10', '2020-12-15 20:22:10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vendor_portfolios`
--

CREATE TABLE `vendor_portfolios` (
  `vendor_portfolio_id` int(10) UNSIGNED NOT NULL,
  `vendor_id` int(10) UNSIGNED NOT NULL,
  `type` enum('1','2','3') NOT NULL DEFAULT '1',
  `portfolio_image` varchar(225) DEFAULT 'NULL',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vendor_portfolios`
--

INSERT INTO `vendor_portfolios` (`vendor_portfolio_id`, `vendor_id`, `type`, `portfolio_image`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 7, '1', 'jAeMded01B_1600232804.jpg', '2020-09-16 01:06:44', '2020-09-16 11:03:57', NULL),
(2, 13, '1', 'WbaAPw8kb5_1602914439.png', '2020-10-17 02:00:40', '2020-10-17 02:00:40', NULL),
(3, 13, '2', 'gtaoKxgoGb_1602914452.png', '2020-10-17 02:00:52', '2020-10-17 02:00:52', NULL),
(4, 20, '1', 'lnwYxebfdK_1604745501.jpg', '2020-11-07 06:38:21', '2020-11-07 06:38:21', NULL),
(5, 20, '2', 'zi7LYvOXju_1604745572.mp4', '2020-11-07 06:39:32', '2020-11-07 06:39:32', NULL),
(6, 31, '1', 'PetbOFuNwY_1605169547.jpg', '2020-11-12 04:25:47', '2020-11-12 04:25:47', NULL),
(7, 31, '1', '9ntTS4So5n_1605169555.jpg', '2020-11-12 04:25:55', '2020-11-12 04:25:55', NULL),
(8, 20, '1', 'gBU82LctN8_1605959918.jpg', '2020-11-21 07:58:38', '2020-11-21 07:58:38', NULL),
(9, 20, '2', 'BYIwl5Cj7z_1606111788.MOV', '2020-11-23 02:09:48', '2020-11-23 02:09:48', NULL),
(10, 20, '1', 'iD6PE7RTTk_1606112115.jpg', '2020-11-23 02:15:15', '2020-11-23 02:15:15', NULL),
(11, 20, '1', 'mjydpjTIuN_1606305390.jpg', '2020-11-25 07:56:30', '2020-11-25 07:56:30', NULL),
(12, 31, '1', 'uK0uQ4YZYn_1606320545.jpg', '2020-11-25 12:09:05', '2020-11-25 12:09:05', NULL),
(13, 31, '1', 'mEDCoZvl8f_1606320552.jpg', '2020-11-25 12:09:12', '2020-11-25 12:09:12', NULL),
(14, 31, '1', 'HosVEZZnkt_1606320559.jpg', '2020-11-25 12:09:19', '2020-11-25 12:09:19', NULL),
(15, 31, '1', 'm89VP7sUKz_1606320567.jpg', '2020-11-25 12:09:27', '2020-11-25 12:09:27', NULL),
(16, 31, '1', 'eCQUfMQFyh_1606320573.jpg', '2020-11-25 12:09:34', '2020-11-25 12:09:34', NULL),
(17, 31, '1', 'iV5EuTUMTJ_1606320622.jpg', '2020-11-25 12:10:22', '2020-11-25 12:10:22', NULL),
(18, 34, '1', '3nY1CTB7Wv_1606394764.jpg', '2020-11-26 08:46:04', '2020-11-26 08:46:04', NULL),
(19, 34, '1', '6s9lwKuKDX_1606394784.jpg', '2020-11-26 08:46:24', '2020-11-26 08:46:24', NULL),
(20, 20, '2', 'SsvCjYUKE1_1606467460.MOV', '2020-11-27 04:57:40', '2020-11-27 04:57:40', NULL),
(21, 41, '1', 'BkeWVSY6Ql_1608047404.jpg', '2020-12-15 11:50:04', '2020-12-15 11:50:04', NULL),
(22, 41, '1', 'tIXCIM4gnw_1608047416.jpg', '2020-12-15 11:50:16', '2020-12-15 11:50:16', NULL),
(23, 41, '1', 'VvedPB9bSS_1608047426.jpg', '2020-12-15 11:50:26', '2020-12-15 11:50:26', NULL),
(24, 41, '1', 'XeA858WVPl_1608047435.jpg', '2020-12-15 11:50:35', '2020-12-15 11:50:35', NULL),
(25, 41, '1', 'H6bHLXSn7i_1608047447.jpg', '2020-12-15 11:50:47', '2020-12-15 11:50:47', NULL),
(26, 41, '2', 'SWqBCyqrME_1608047484.MOV', '2020-12-15 11:51:24', '2020-12-15 11:51:24', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vendor_ratings`
--

CREATE TABLE `vendor_ratings` (
  `vendor_rating_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `vendor_id` int(10) UNSIGNED NOT NULL,
  `appointment_id` int(10) UNSIGNED DEFAULT NULL,
  `rating` int(10) UNSIGNED NOT NULL,
  `review` varchar(225) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `vendor_ratings`
--

INSERT INTO `vendor_ratings` (`vendor_rating_id`, `user_id`, `vendor_id`, `appointment_id`, `rating`, `review`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 6, 7, 1, 5, 'Best user', '2020-09-19 11:53:17', '2020-09-28 13:21:23', NULL),
(2, 6, 7, 1, 4, 'This this', '2020-09-26 01:50:03', '2020-09-26 01:50:03', NULL),
(3, 6, 7, 1, 2, 'This this', '2020-09-26 01:50:03', '2020-09-28 13:33:11', NULL),
(4, 6, 20, 1, 2, 'This this', '2020-09-26 01:50:03', '2020-09-28 13:33:11', NULL),
(5, 7, 20, 1, 2, 'This this', '2020-09-26 01:50:03', '2020-09-28 13:33:11', NULL),
(6, 28, 20, 7, 3, 'dfsdf', '2020-11-17 03:47:51', '2020-11-17 03:47:51', NULL),
(7, 28, 20, 11, 2, 'Fgg', '2020-11-18 01:44:43', '2020-11-18 01:44:43', NULL),
(8, 28, 20, 19, 3, 'test ger', '2020-11-25 01:25:58', '2020-11-25 01:25:58', NULL),
(9, 13, 31, 21, 3, 'It was great experience', '2020-11-25 13:51:22', '2020-11-25 13:51:22', NULL),
(10, 13, 31, 12, 5, '👍🏽', '2020-11-25 21:26:36', '2020-11-25 21:26:36', NULL),
(11, 35, 34, 24, 5, 'Good', '2020-11-26 08:52:15', '2020-11-26 08:52:15', NULL),
(12, 28, 20, 34, 3, 'Good', '2020-12-08 02:26:05', '2020-12-08 02:26:05', NULL),
(13, 28, 20, 36, 1, 'Bad experibce', '2020-12-11 06:22:16', '2020-12-11 06:22:16', NULL),
(14, 6, 20, 37, 4, 'Not good', '2020-12-14 05:32:55', '2020-12-14 05:32:55', NULL),
(15, 28, 20, 43, 5, 'Perfect', '2020-12-15 09:13:49', '2020-12-15 09:13:49', NULL),
(16, 42, 41, 44, 5, 'Ffffhhjjkk', '2020-12-15 15:37:37', '2020-12-15 15:37:37', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vendor_services`
--

CREATE TABLE `vendor_services` (
  `vendor_service_id` int(10) UNSIGNED NOT NULL,
  `vendor_id` int(10) UNSIGNED NOT NULL,
  `service_for` enum('1','2') NOT NULL DEFAULT '1' COMMENT '1=men,2=women',
  `category_id` int(10) UNSIGNED NOT NULL,
  `service_name` varchar(225) NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_bin,
  `service_image` varchar(255) DEFAULT NULL,
  `service_cost` int(11) DEFAULT '0',
  `booking_amount` int(11) DEFAULT '0',
  `rate` decimal(10,2) NOT NULL,
  `time_slots` varchar(225) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vendor_services`
--

INSERT INTO `vendor_services` (`vendor_service_id`, `vendor_id`, `service_for`, `category_id`, `service_name`, `description`, `service_image`, `service_cost`, `booking_amount`, `rate`, `time_slots`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 8, '1', 1, 'Ganeshay1', NULL, NULL, 0, 0, 10.00, '10', '2020-09-15 03:59:52', '2020-10-30 13:25:50', NULL),
(2, 7, '1', 1, 'Ganeshay1', NULL, NULL, 0, 0, 10.00, '10', '2020-09-15 03:59:52', '2020-10-26 07:06:08', NULL),
(3, 7, '1', 1, 'Ganeshay', NULL, NULL, 0, 0, 10.00, '10', '2020-10-25 21:35:12', '2020-10-25 21:35:12', NULL),
(4, 7, '1', 1, 'Ganeshay', NULL, NULL, 0, 0, 10.00, '10', '2020-10-25 21:36:51', '2020-10-25 21:36:51', NULL),
(5, 8, '1', 1, 'Ganeshay', NULL, NULL, 0, 0, 10.00, '10', '2020-10-25 21:37:26', '2020-10-30 13:19:28', NULL),
(6, 8, '1', 1, 'Ganeshay', NULL, NULL, 0, 0, 10.00, '10', '2020-10-25 21:38:05', '2020-10-30 13:19:23', NULL),
(7, 17, '1', 3, 'hair cutting', NULL, NULL, 0, 0, 123.00, NULL, '2020-10-31 04:51:39', '2020-10-31 04:51:39', NULL),
(8, 17, '2', 5, 'face clienning', NULL, NULL, 0, 0, 50.00, NULL, '2020-10-31 05:30:29', '2020-11-17 06:11:03', NULL),
(9, 18, '1', 3, 'Hair cutting', NULL, NULL, 0, 0, 12.00, NULL, '2020-10-31 07:11:16', '2020-10-31 07:11:16', NULL),
(10, 20, '1', 3, 'Geyhe', NULL, NULL, 0, 0, 123.00, NULL, '2020-11-04 05:23:21', '2020-11-06 02:58:39', '2020-11-06 02:58:39'),
(11, 22, '1', 1, 'Mm', NULL, NULL, 0, 0, 6.00, NULL, '2020-11-04 10:06:25', '2020-11-04 10:06:25', NULL),
(12, 20, '1', 4, 'Ahh', NULL, 'ZeajjuiMzQ_1604654842.jpg', 0, 0, 12.00, NULL, '2020-11-06 04:27:22', '2020-11-21 06:51:45', '2020-11-21 06:51:45'),
(13, 20, '1', 3, 'Service 1', NULL, 'jEXGMBIjMD_1604738061.jpg', 0, 0, 123.00, NULL, '2020-11-07 03:34:21', '2020-11-17 06:15:02', '2020-11-02 23:00:00'),
(14, 31, '2', 3, 'Hairstyle', NULL, 'nbxAcdL6Zm_1606320772.jpg', 0, 0, 242.00, NULL, '2020-11-12 03:23:37', '2020-11-25 11:12:52', NULL),
(15, 31, '2', 5, 'Makeup', NULL, 'jbXod0QEa5_1606320740.jpg', 0, 0, 302.50, NULL, '2020-11-19 19:07:56', '2020-11-25 11:12:20', NULL),
(16, 31, '2', 11, 'Lips', NULL, '5hs3MnyoDO_1606320706.jpg', 0, 0, 121.00, NULL, '2020-11-19 19:08:50', '2020-11-25 11:11:46', NULL),
(17, 31, '2', 9, 'Tattoos', NULL, 'ZS3qCBGcXY_1605831010.jpg', 0, 0, 660.00, NULL, '2020-11-19 19:10:10', '2020-11-25 20:38:30', '2020-11-25 20:38:30'),
(18, 20, '1', 4, 'Saloon', NULL, '8gCLRXw186_1605866783.jpg', 56, 6, 61.60, NULL, '2020-11-20 05:06:23', '2020-12-18 01:44:42', NULL),
(19, 20, '2', 2, 'Beauty', NULL, 'T4vqvCQ8yk_1606305251.jpg', 0, 0, 110.00, NULL, '2020-11-25 06:54:11', '2020-11-25 06:54:11', NULL),
(20, 33, '1', 3, 'Hair Cut', NULL, NULL, 0, 0, 50.00, NULL, '2020-11-25 09:38:56', '2020-11-25 09:38:56', NULL),
(21, 34, '1', 1, 'Hair cuting', NULL, NULL, 0, 0, 20.00, NULL, '2020-11-26 00:42:17', '2020-11-26 00:42:17', NULL),
(22, 34, '1', 5, 'Nail spa', NULL, 'J7ducoSuvs_1606369817.jpg', 0, 0, 495.00, NULL, '2020-11-26 00:50:17', '2020-11-26 00:55:35', '2020-11-26 00:55:35'),
(23, 34, '2', 5, 'Face bridal womens', NULL, 'FbJ5TpIZ45_1606394488.jpg', 0, 0, 658.90, NULL, '2020-11-26 07:41:28', '2020-11-26 07:41:28', NULL),
(24, 31, '2', 10, 'Nails', NULL, 'RKQURll3eu_1606497541.jpg', 0, 0, 110.00, NULL, '2020-11-27 12:19:01', '2020-11-27 12:19:01', NULL),
(25, 20, '2', 3, 'Hair red color', NULL, '5CpRviqKHc_1607408032.jpg', 50, 5, 55.00, NULL, '2020-12-08 01:13:52', '2020-12-08 01:13:52', NULL),
(26, 20, '2', 5, 'Nail colour', NULL, 'tgdgHWLGi7_1607408404.jpg', 100, 10, 110.00, NULL, '2020-12-08 01:20:04', '2020-12-08 01:20:04', NULL),
(27, 20, '1', 2, 'Hair cuting', NULL, 'ap8etRIy0e_1607681899.jpg', 190, 19, 209.00, NULL, '2020-12-11 05:18:19', '2020-12-11 05:18:19', NULL),
(28, 20, '1', 3, 'Hair was', NULL, '7JGFDvXK7f_1607938175.jpg', 199, 20, 218.90, NULL, '2020-12-14 04:29:35', '2020-12-14 04:29:35', NULL),
(29, 39, '1', 1, 'Codemeg Silver', NULL, 'ASvvpNAMDz_1607946637.jpg', 100, 10, 110.00, NULL, '2020-12-14 06:50:37', '2020-12-14 06:50:37', NULL),
(30, 39, '1', 1, 'Codemeg Gold', NULL, 'PX9nJET7cM_1607946870.jpg', 150, 15, 165.00, NULL, '2020-12-14 06:54:30', '2020-12-14 06:54:30', NULL),
(31, 39, '1', 3, 'Hair cut with colur', NULL, 'bfKmN54zUz_1607947211.jpg', 299, 30, 328.90, NULL, '2020-12-14 07:00:11', '2020-12-14 07:00:11', NULL),
(32, 20, '2', 5, 'Facail plus hair wash', NULL, 'tX900mtnMx_1608037769.jpg', 499, 50, 548.90, NULL, '2020-12-15 08:09:29', '2020-12-15 08:09:29', NULL),
(33, 41, '2', 2, 'Makeup', NULL, 'ZOKU4Q12vA_1608044650.jpg', 100, 10, 110.00, NULL, '2020-12-15 10:04:10', '2020-12-15 10:04:10', NULL),
(34, 41, '2', 3, 'Hairstyle', NULL, 'PKan4bhaFk_1608045059.jpg', 200, 20, 220.00, NULL, '2020-12-15 10:10:59', '2020-12-15 10:10:59', NULL),
(35, 20, '2', 3, 'Nali palish', NULL, '1MsLEfUbB8_1608272130.jpg', 190, 21, 229.90, NULL, '2020-12-18 01:15:30', '2020-12-18 01:43:07', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `appointments`
--
ALTER TABLE `appointments`
  ADD PRIMARY KEY (`appointment_id`);

--
-- Indexes for table `app_ratings`
--
ALTER TABLE `app_ratings`
  ADD PRIMARY KEY (`app_rating_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `chat`
--
ALTER TABLE `chat`
  ADD PRIMARY KEY (`chat_id`);

--
-- Indexes for table `commissions`
--
ALTER TABLE `commissions`
  ADD PRIMARY KEY (`commission_id`);

--
-- Indexes for table `days`
--
ALTER TABLE `days`
  ADD PRIMARY KEY (`day_id`);

--
-- Indexes for table `documents`
--
ALTER TABLE `documents`
  ADD PRIMARY KEY (`document_id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`notification_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`(191));

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `supports`
--
ALTER TABLE `supports`
  ADD PRIMARY KEY (`support_id`);

--
-- Indexes for table `transaction_history`
--
ALTER TABLE `transaction_history`
  ADD PRIMARY KEY (`transaction_history_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor_days`
--
ALTER TABLE `vendor_days`
  ADD PRIMARY KEY (`vendor_day_id`);

--
-- Indexes for table `vendor_portfolios`
--
ALTER TABLE `vendor_portfolios`
  ADD PRIMARY KEY (`vendor_portfolio_id`);

--
-- Indexes for table `vendor_ratings`
--
ALTER TABLE `vendor_ratings`
  ADD PRIMARY KEY (`vendor_rating_id`);

--
-- Indexes for table `vendor_services`
--
ALTER TABLE `vendor_services`
  ADD PRIMARY KEY (`vendor_service_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `appointments`
--
ALTER TABLE `appointments`
  MODIFY `appointment_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `app_ratings`
--
ALTER TABLE `app_ratings`
  MODIFY `app_rating_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `chat`
--
ALTER TABLE `chat`
  MODIFY `chat_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `commissions`
--
ALTER TABLE `commissions`
  MODIFY `commission_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `days`
--
ALTER TABLE `days`
  MODIFY `day_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `documents`
--
ALTER TABLE `documents`
  MODIFY `document_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `notification_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `supports`
--
ALTER TABLE `supports`
  MODIFY `support_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `transaction_history`
--
ALTER TABLE `transaction_history`
  MODIFY `transaction_history_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `vendor_days`
--
ALTER TABLE `vendor_days`
  MODIFY `vendor_day_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=197;

--
-- AUTO_INCREMENT for table `vendor_portfolios`
--
ALTER TABLE `vendor_portfolios`
  MODIFY `vendor_portfolio_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `vendor_ratings`
--
ALTER TABLE `vendor_ratings`
  MODIFY `vendor_rating_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `vendor_services`
--
ALTER TABLE `vendor_services`
  MODIFY `vendor_service_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
