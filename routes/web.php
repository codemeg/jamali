<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Auth::routes();
Route::get('/admin', 'Auth\LoginController@showLoginForm')->name('admin-login');
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/demo', 'Admin\UserController@index')->name('index');

Route::get('users', 'Admin\UserController@index');
Route::get('users-list', 'Admin\UserController@usersList'); 
Route::get('term_condition', 'Admin\HomeController@termCondition')->name('term_condition');

Route::get('payment', 'PaymentController@paymentForm')->name('payment.form');
Route::post('payment', 'PaymentController@payment');
Route::get('payment/callback', 'PaymentController@paymentCallBack')->name('payment.callback');

Route::get('api/user/success', function(){
     return view('paypal.success');
})->name('payment.success');

Route::get('api/user/cancel', function(){
    return view('paypal.cancel');
})->name('payment.failed');

Route::get('api/user/error', function(){
    return view('paypal.cancel');
})->name('payment.error');

/**
 *  Landing Page
 */
Route::get('/', 'HomeController@index')->name('index');
Route::get('/term-condition', 'HomeController@termCondition')->name('term.condition');
Route::get('/privacy-policy', 'HomeController@privacyPolicy')->name('privacy.policy');
Route::get('/support', 'HomeController@support')->name('support');


